 %! TEX root = master.tex
\chapter[Diferenciales cuadr\'aticas en el plano complejo][Diferenciales cuadr\'aticas en el plano]{Diferenciales cuadr\'aticas en el plano complejo}\label{difplano}

En este cap\'itulo introducimos la version \emph{local} de los objetos centrales de este trabajo: las \emph{diferenciales cuadr\'aticas}. El punto de vista que tomaremos es que \'estas son un marco conveniente en el que se  puede hablar de ecuaciones diferenciales holomorfas en el plano complejo y la estructura geom\'etrica de las soluciones. Por este motivo, comenzaremos diciendo a qu\'e nos referimos por ecuaci\'on diferencial holomorfa en el plano complejo y qu\'e es una soluci\'on de \'esta.

\section{Ecuaciones diferenciales en el plano complejo}


\begin{definicion} 
Un campo vectorial holomorfo en un abierto $\mathcal{U}$ del plano complejo $\mathbb{C}$ es una funci\'on holomorfa ${\mathcal{U}\xrightarrow{F}\mathbb{C}}$.
 \end{definicion} 

Todo campo vectorial en $\mathbb{C}$ determina una ecuaci\'on diferencial en el plano complejo. Esta ecuaci\'on se puede escribir concisamente como:
\begin{equation}
\label{dif-campovect}
D\gamma(t) =  F(\gamma(t))
\end{equation}
donde $\gamma:\mathcal{I}\rightarrow\mathbb{C}$ (con $\mathcal{I}\subseteq \mathbb{R}$ un intervalo abierto), es decir, la soluci\'on a la ecuaci\'on determinada por $F$ est\'a dada por una curva $\gamma$ tangente al campo en todo punto en el que \'esta est\'a definida. A las soluciones de la ecuaci\'on asociada a $F$ les llamaremos \emph{curvas integrales} de $F$.

En lo que resta del cap\'itulo estaremos interesados en estudiar la <<forma>> de las curvas integrales de campos holomorfos; no nos detendremos a explicar detalladamente a qu\'e nos referimos por la <<forma>> de las curvas, sin embargo, m\'as adelante explicaremos qu\'e quiere decir clasificar \emph{localmente} la forma de \'estas y en el \'ultimo cap\'itulo de la parte 1 retomaremos el problema de entender la <<forma global>> de las curvas integrales.

Si $x$ es un punto en el que $F$ se anula, entonces la \'unica curva integral de $F$ que pasa por $x$ es la curva constante:
$$\gamma(t)=x~~~~~~~ \forall t$$
y de cierto modo, estas son las curvas integrales m\'as simples que hay, por lo que nos tomaremos la libertad de omitirlas de nuestro estudio, es decir, estudiaremos las curvas integrales de $F$ en el abierto $\{z\in\mathcal{U}| F(z)\neq 0\}$. En particular, esta restricci\'on es prec\'isamente suponer que que $F$ no se anula, por lo que podemos reescribir la ecuaci\'on \eqref{dif-campovect} como:
\begin{equation} 
\left(F(\gamma(t)\right)^{-1}D\gamma(t) =1.
 \end{equation} 
Si definimos un nuevo campo como $\phi(x):=F(x)^{-1}$ entonces tenemos
\begin{equation}
\label{abeliana-campo}
\phi(\gamma(t))D\gamma(t) =1.
 \end{equation}
El lado izquierdo de la \'ultima ecuaci\'on deber\'ia evocarnos la expresi\'on para la imagen transpuesta \pullBack de una $1$-forma bajo $\gamma$. De hecho tenemos
$$\phi(\gamma(t))D\gamma(t)dt=\gamma^*(\phi(z)dz).$$
Por lo que encontrar una soluci\'on $\gamma$ de la ecuaci\'on \eqref{abeliana-campo} es equivalente a encontrar una soluci\'on $\gamma$ de $\gamma^*(\phi(z)dz)=dt$.
\begin{definicion} 
A una $1$-forma $\omega=\phi(z)dz$ con $\phi$ una funci\'on holomorfa le llamaremos una \emph{diferencial abeliana}. A una curva $\gamma$ que satisfaga $\gamma^*(\omega)=dt$ (o equivalentemente la ecuaci\'on \eqref{abeliana-campo}) le llamaremos \emph{trayectoria horizontal unitaria}\footnote{Esta definici\'on no es del todo est\'andar, sin embargo la usaremos provisionalmente para distinguir a estas trayectorias de las \emph{trayectorias horizontales}.\attention} de $\omega$.
 \end{definicion} 

\begin{ejemplo} 
Consideremos la diferencial abeliana $dz$, es decir, la diferencial que resulta de tomar la funci\'on constante $1$. En este caso las trayectorias horizontales unitarias son de la forma $\gamma(t)=z_0 +t$.

Si $\gamma$ es cualquier trayectoria horizontal unitaria, y $\psi(t)=2t$, entonces $\gamma\circ\psi$ es una reparametrizaci\'on de la curva $\gamma$. Sin embargo, esta nueva curva ya no es una trayectoria horizontal unitaria, pues $\phi(\gamma\circ\psi(t))D\gamma\circ\psi(t)=D\gamma(2t)\cdot 2=2$.
 \end{ejemplo} 

\missingfigure{Dibujo del campo determinado por dz una soluci\'on, y la reparametrizacion que no lo es}

El ejemplo anterior muestra que las reparametrizaciones de trayectorias horizontales unitarias no forzosamente son horizontales unitarias, lo cual es algo indeseable pues solo estamos interesados en la <<forma>> de las trayectorias y no en la parametrizaci\'on particular que las describa; es por esto que, en vez de estudiar curvas que sean soluci\'on de \eqref{abeliana-campo}, estudiaremos curvas que satisfagan 

\begin{equation} 
\label{dif-abel}
\phi(\gamma(t))D\gamma(t)\in \mathbb{R}_{>0}.
 \end{equation} 
Nuevamente, usando el lenguaje de formas diferenciales, esto es equivalente a que la curva $\gamma$ satisfaga $\gamma^*(\phi(z)dz)=f(t)dt$ donde $f$ es una funci\'on que toma valores reales positivos. A dichas curvas les llamaremos \emph{trayectorias horizontales} de $\omega=\phi(z)dz$.

\begin{proposicion} 
Si $\gamma_1$ es una trayectoria horizontal de $\omega$ y $\gamma_2$ es una  reparametrizaci\'on con la misma orientaci\'on, entonces tambi\'en es una trayectoria horizontal.
 \end{proposicion} 
\begin{proof}
Digamos que $\gamma_2=\gamma\circ\psi$; el hecho de que $\gamma_1$ y $\gamma_2$ tengan la misma orientaci\'on es equivalente a que $D\psi(t)$ sea real y positivo para todo $t$. Entonces $(\gamma_2)^*(\omega)=\psi^*(\gamma_1^*(\omega))=\psi^*(f(t)dt)=f(\psi(t))D\psi dt$ y como $(f\circ\psi) D\psi>0$ concluimos  que $\gamma_2$ es horizontal.
\end{proof}

\begin{ejemplo}\label{ejemplo-abel-z}
Consideremos la diferencial abeliana $\omega=zdz$. En este caso es f\'acil describir expl\'icitamente algunas trayectorias horizontales. Por ejemplo, si $\gamma$ es una trayectoria horizontal que pasa por un punto $p$ del semi-eje real positivo, digamos $\gamma(t_0)=p$, entonces $pD\gamma(t_0)=\gamma(t_0)D\gamma(t_0)>0$ por lo que $D\gamma(t_0)>0$. Con esta observaci\'on no es dif\'icil convencerse de que la curva $\gamma(t)=\lambda t$ es una trayectoria horizontal para todo $\lambda>0$ y $t>0$. Con un an\'alisis similar se puede ver que los semi-ejes real negativo, imaginario positivo, e imaginario negativo, con las orientaciones como en la figura \ref{fig:tray-zdz-algunas}, son trayectorias horizontales.
\end{ejemplo} 
\begin{figure}
\label{fig:tray-zdz-algunas}
\centering
\begin{tikzpicture}[scale=2]
\draw [->-=1/3, ->-=2/3, ->-=1] (0,0) -- (1,0) node[below] {$1$};
\draw [->-=1/3, ->-=2/3, ->-=1] (0,0) -- (-1,0) node[below] {$-1$};
\draw [->-=1/3, ->-=2/3, ->-=1] (0,0) -- (0,1) node[above] {$i$};
\draw [->-=1/3, ->-=2/3, ->-=1] (0,0) -- (0,-1) node[below] {$-i$};
\end{tikzpicture}
\caption{Algunas trayectorias horizontales de la diferencial abeliana $zdz$}
\end{figure}

Usar este tipo de m\'etodos para encontrar las trayectorias horizontales que pasan por otros puntos se vuelve inasequible incluso para esta diferencial abeliana, que evidentemente es de las m\'as simples. Posteriormente desarrollaremos m\'etodos m\'as sofisticados que nos permitir\'an encontrar las trayectorias horizontales que pasan por cualquier punto.


\begin{ejemplo} 
Nuevamente consideremos la diferencial abeliana m\'as simple de todas: $\omega=dz$. En este caso la trayectoria $\gamma(t)=z_0 -t$ es una reparametrizaci\'on (que invierte orientaci\'on) de una trayectoria horizontal, pero no es horizontal. \end{ejemplo} 


Para garantizar que cualquier reparametrizaci\'on de una trayectoria horizontal sea horizontal (sin importar si la reparametrizaci\'on tiene o no la misma orientaci\'on que la trayectoria original) podemos hacer varias modificaciones a la ecuaci\'on \eqref{dif-abel}, la m\'as evidente es:


$$\phi(\gamma(t))D\gamma(t)\neq0.$$
Esta condici\'on sobre $\gamma$ es equivalente a 
$$\phi(\gamma(t))^2(D\gamma(t))^2\in\mathbb{R}_{>0},$$
que por analog\'ia con la imagen transpuesta \pullBack de $1$-formas podr\'iamos considerar como la <<imagen transpuesta\pullBack>> de la expresi\'on
$$\phi(z)^2dz^2$$
bajo la curva $\gamma$. Finalmente, podemos pensar que la expresi\'on ${\phi(z)^2dz^2}$ es un caso particular de $\psi(z)dz^2$ con $\psi=\phi^2$ una funci\'on holomorfa.



%Sin embargo, m\'as adelante veremos que esto realmente es una soluci\'on aparente del problema (esta soluci\'on corresponde a considerar soluciones de la ecuaci\'on \ref{dif-abel} y posteriormente olvidar su orientaci\'on, por lo que en particular, sigue existiendo una manera global consistente de orientar las soluciones)\todo{explicar m\'as}

%Antes de ver la otra forma posible de proceder, consideremos 
%\todo{cuadrado de una dif abeliana}

\section{Diferenciales cuadr\'aticas}
Inspirados en las ideas de la secci\'on anterior, en esta secci\'on formalizamos la idea de diferencial cuadr\'atica y las trayectorias que determina.

\begin{definicion} 
Una \emph{diferencial cuadr\'atica} sobre $\mathcal{U}$ (con $\mathcal{U}\subseteq \mathbb{C}$ un abierto del plano complejo) es una expresi\'on de la forma
$$f(z)dz^2$$
donde $f(z)$ es una funci\'on holomorfa sobre el abierto $\mathcal{U}$.
 \end{definicion} 

\begin{definicion} 
Una \emph{trayectoria horizontal} de la diferencial cuadr\'atica $\theta=f(z)dz^2$ es una curva $\mathcal{I}\xrightarrow{\gamma}\mathcal{U}$\attention{} tal que para todo $t\in\mathcal{I}$ se tiene que:
$$f(\gamma(t))(D\gamma(t))^2>0$$
 \end{definicion} 

\begin{proposicion} 
Si $\gamma_1$ es una trayectoria horizontal de $\theta=f(z)dz^2$ y $\gamma_2$ es una reparametrizaci\'on de $\gamma_1$, entonces $\gamma_2$ tambi\'en es una trayectoria horizontal de $\theta$.
 \end{proposicion} 
\begin{proof} 
La demostraci\'on es simplemente una formalizaci\'on de las ideas de la secci\'on pasada.
 \end{proof} 

Una herramienta muy \'util al estudiar ecuaciones diferenciales es usar transformaciones de estas que las lleven a formas m\'as faciles de comprender.


\begin{definicion} 
Sea $\theta=f(z)dz^2$ una diferencial cuadr\'atica sobre un abierto $\mathcal{U}$ y $\mathcal{V}\xrightarrow{\phi}\mathcal{U}$\attention{} una transformaci\'on holomorfa. La imagen transpuesta \pullBack de $\theta$ bajo $\phi$ es la diferencial cuadr\'atica sobre $\mathcal{V}$ dada por:
$$\phi^*(\theta):=f(\phi(z))(D\phi(z))^2dz^2.$$
 \end{definicion} 

Hay dos maneras de justificar esta definici\'on: la primera es que este es un an\'alogo de la imagen transpuesta \pullBack de $1$-formas diferenciales. La otra raz\'on, acaso m\'as significativa, es que esta definici\'on es la que nos permite demostrar la siguiente proposici\'on, misma que, a grandes razgos, permite encontrar las trayectorias horizontales de $\theta$ en t\'erminos de las trayectorias horizontales de la imagen transpuesta \pullBack de $\theta$.
\begin{proposicion}\label{trayectorias-pullback-plano} 
Sea $\phi$ una transformaci\'on holomorfa de $\mathcal{V}$ en $\mathcal{U}$ y $\theta$ una diferencial cuadr\'atica en $\mathcal{U}$; si $\gamma$ es una trayectoria de $\phi^*(\theta)$ entonces $\phi\circ\gamma$ es una trayectoria de $\theta$.
 \end{proposicion}
\begin{proof} 
Es una aplicaci\'on de la regla de la cadena.\attention
 \end{proof} 


\begin{ejemplo} 
Consideremos la diferencial cuadr\'atica $\theta=zdz^2$. Al igual que en el ejemplo \ref{ejemplo-abel-z} no es dif\'icil convencerse de que los rayos que pasan por las tres ra\'ices de la unidad son trayectorias horizontales de $\theta$. Para encontrar las dem\'as trayectorias horizontales usaremos la proposici\'on anterior.

Consideremos la transformaci\'on $\phi(z)=z^{\frac23}$ que es un biholomorfismo entre el semiplano superior y la regi\'on $\{w~|~0<arg(w)<\frac{2\pi}{3}\}$. Un sencillo c\'alculo muestra que $\phi^*(zdz^2)=dz^2$. Si $\gamma$ es cualquier recta horizontal del semi-plano superior, entonces $\gamma$ es una trayectoria horizontal de $dz^2$ y por la proposici\'on anterior tenemos que $\phi\circ\gamma$ es una trayectoria horizontal de $zdz^2$.\attention{}

A partir de esto se puede encontrar expl\'icitamente la forma de las trayectorias horizontales (fig. \ref{fig:zdz2})
 \end{ejemplo} 
\begin{figure}[h]
\label{fig:zdz2}
\centering
\input{figuras/sectores/sector3.pgf}
\caption{Trayectorias horizontales de $zdz^2$}
\end{figure}
\ptodo{el efecto de $\phi$ en las rectas horizontales}

%\begin{secundario}
\subsection{El caso de las diferenciales abelianas}
Recordemos que ya hemos hablado de la imagen transpuesta \pullBack de diferenciales abelianas. En este caso el resultado an\'alogo a la proposici\'on \ref{trayectorias-pullback-plano} es cierto tambi\'en.
%\begin{definicion} 
%Sea $\omega=f(z)dz$ una diferencial abeliana sobre un abierto $\mathcal{U}$ y $\mathcal{V}\xrightarrow{\phi}\mathcal{U}$ una transformaci\'on holomorfa. El \pullback\  de $\omega$ bajo $\phi$ es la diferencial abeliana sobre $\mathcal{V}$ dada por:
%$$\phi^*(\omega):=f(\phi(z))(\phi^\prime(z))dz$$
%\end{definicion}
\begin{proposicion}
Sea $\phi$ una transformaci\'on holomorfa de $\mathcal{V}$ en $\mathcal{U}$ y $\omega$ una diferencial abeliana en $\mathcal{U}$; si $\gamma$ es una trayectoria horizontal de $\phi^*(\omega)$ entonces $\phi\circ\gamma$ es una trayectoria de $\omega$
\end{proposicion}
\begin{proof} 
La demostraci\'on es totalmente an\'aloga al caso de diferenciales cuadr\'aticas.
\end{proof}
Otro aspecto que surge de las ideas de la secci\'on pasada es el hecho de que hay una manera de asociar a toda diferencial abeliana una diferencial cuadr\'atica:
\begin{definicion} 
El \emph{cuadrado} de una diferencial abeliana $\omega=f(z)dz$ es la diferencial cuadr\'atica $w^2=f(z)^2dz^2$
\end{definicion} 
Impl\'icito tambi\'en est\'a el hecho de que las trayectorias horizontales de $\omega^2$ son simplemente reparametrizaciones de las trayectorias horizontales de $\omega$ (posiblemente con la orientaci\'on opuesta).\attention

Al ver esto, uno podr\'ia preguntarse si existe una operaci\'on inversa que asocie diferenciales abelianas a las diferenciales cuadr\'aticas, digamos, un tipo de <<ra\'iz cuadrada>>. Sin embargo, existen diferenciales cuadr\'aticas que no son el cuadrado de ninguna diferencial abeliana (esto tiene que ver con que no toda funci\'on holomorfa tiene ra\'iz cuadrada) v\'ease \ref{cuadratica-no-abel}.

En cierta forma, es esto lo que hace la teor\'ia de diferenciales cuadr\'aticas m\'as diversa que su contraparte en el caso abeliano. %Bajo ciertas hip\'otesis constuiremos la ra\'iz cuadrada de una diferencial cuadr\'atica \emph{localmente} en la secci\'on \ref{clasificacion-dif-local}.
Retomaremos estas idea en las secciones \ref{raiz-global}.
\ftodo
%\end{secundario} 

\section[Clasificaci\'on local de las trayectorias de diferenciales cuadr\'aticas][Clasificaci\'on local de las trayectorias]{Clasificaci\'on local de las trayectorias de diferenciales cuadr\'aticas}

El primer problema concerniente a la clasificaci\'on de la <<forma>> de las trayectorias horizontales de las diferenciales cuadraticas (abelianas) que resulta accesible es la clasificaci\'on local. A continuaci\'on diremos exactamente qu\'e significa esto.

\begin{definicion}
Si $\theta$ y $\eta$ son dos diferenciales cuadr\'aticas sobre $\mathcal{U}$ y $\mathcal{V}$ respectivamente, $p\in\mathcal{U}$ y $q\in\mathcal{V}$ dos puntos arbitrarios, diremos que las trayectorias horizontales de $\theta$ tienen una \emph{forma equivalente} a la forma de las trayectorias de $\eta$ \emph{localmente} al rededor de $p$ y $q$ si existe un difeomorfismo de una vecindad de $p$ a $q$, que manda $p$ a $q$ y que manda las trayectorias horizontales de $\theta$ en las trayectorias horizontales de $\eta$.\attention
 \end{definicion}
\begin{observacion}
Por la proposici\'on \ref{trayectorias-pullback-plano}, para demostrar que dos diferenciales cuadr\'aticas $\theta$ y $\eta$ tienen forma equivalente al rededor de dos puntos $p$ y $q$, basta encontrar un biholomorfismo $\phi$ que mande $p$ a $q$ y tal que $\phi^*(\eta)=\theta$.
 \end{observacion}

Hay dos comportamientos radicalmente distintos en las trayectorias, por un lado, al rededor de puntos regulares el comportamiento es muy simple y esencialmente \'unico; por otro lado, al rededor de los puntos cr\'iticos el comportamiento es m\'as complicado y ya no es \'unico.

\begin{definicion} 
Dada una diferencial cuadr\'atica $\omega=f(z)dz^2$ un punto $p\in \mathcal{U}$ es \emph{regular} si $f(p)\neq0$ y \emph{cr\'itico} si no es regular\footnote{Evitamos definir punto cr\'itico como aquellos puntos en los que $f(p)=0$ pues m\'as adelante, al introducir la noci\'on de diferencial cuadr\'atica \emph{meromorfa}, veremos que los polos de la funci\'on tambi\'en son puntos cr\'iticos.}.
 \end{definicion} 

\begin{ejemplo} 
Las trayectorias horizontales de la diferencial cuadr\'atica $dz^2$ son las rectas horizontales.
 \end{ejemplo} 
\begin{proposicion} 
Si $p$ es un punto regular de la diferencial ${\theta=f(z)dz^2}$ entonces hay un biholomorfismo $\phi$ tal que
\begin{align*}
\phi^*\!\left(dz^2\right) &=\theta\\
%\left(\phi^{-1}\right)^*\!(\theta) &=dz^2
\end{align*}
 \end{proposicion} 
\begin{proof} 
Supongamos que $\phi$ es un biholomorfismo que satisface la primera ecuaci\'on. Esto quiere decir que
$$(\phi^\prime)^2dz^2=\phi^*(dz^2)=f(z)dz^2$$
por lo que $(\phi^\prime)^2=f(z)$ y es claro que esta es una condici\'on suficiente para que $\phi$ satisfaga ambas condiciones.

Como $p$ es un punto regular de $\theta$, es decir, $f(p)\neq 0$, podemos encontrar una vecindad de $p$ en la que la funci\'on $\sqrt{f(z)}$ est\'a bien definida y adem\'as si definimos $\phi$ como
$$\phi(z):=\int_p^z\sqrt{f(w)}dw$$
est\'a bien definida, es holomorfa y satisface $(\phi^\prime)^2dz^2=f(z)dz^2$, por lo que $\phi(p)\neq0$ y $\phi$ es un biholomorfismo en una vecindad posiblemente m\'as peque\~na de $p$.\ftodo
 \end{proof} 
\begin{corolario} 
En el abierto en el que est\'a definido $\phi$ las trayectorias de $\theta$ est\'an dadas por la imagen de las rectas horizontales bajo $\phi^{-1}$.
 \end{corolario}

%La siguiente proposici\'on nos permite expresar \ftodo

\begin{proposicion} \label{prop-clasif-ceros-dq}
Si $p$ es un punto cr\'itico de la diferencial ${\theta=f(z)dz^2}$ entoces existe un biholomorfismo $\phi$ tal que
%$$(\phi^{-1})^*\!\left(\theta\right) =\left(\frac{n+2}{2}\right)^2z^ndz^2$$
$$\phi^*\! \left(\left(\frac{n+2}{2}\right)^2z^ndz^2\right)=\theta$$
donde $f(z)=(z-p)^ng(z)$ con $g$ holomorfa y $g(p)\neq0$ \ie $p$ es un cero de orden $n$ de $f$.
 \end{proposicion} 
\begin{proof} 
Dado que las traslaciones tienen derivada 1, si $\tau$ es una traslaci\'on que lleva el $0$ a $p$, entonces $\tau^*(\theta)=f(\tau(z))dz^2$, por lo que sin p\'erdida de generalidad podemos asumir que $p=0$. Con esta suposici\'on tenemos que $f(z)=z^ng(z)$ donde $g$ no se anula en el $0$, por lo que (al menos en una vecindad del $0$) existe una funci\'on holomorfa $\sqrt g$ tal que $(\sqrt g)^2=g$; como $\sqrt g$ no puede anularse en $0$, entonces tiene una expansi\'on en serie de potencias de la forma:
$$\sqrt g (z)=\sum a_iz^i$$
donde $a_0\neq0$. 
Ahora consideremos la funci\'on $h$ cuya expansi\'on en serie de potencias es de la forma
$$h(z)=\sum \frac{a_i}{\frac n2 +i+1}z^i$$
por la observaci\'on anterior $h$ es holomorfa y no nula en una vecindad del cero por lo que la funci\'on $\phi(z)=zh^{\frac 2 {n+2}}$ est\'a bien definida y tiene derivada no nula en $0$, por lo que es un biholomorfismo en una vecindad del cero.
Calculemos la imagen transpuesta \pullBack de $\left(\left(\frac{n+2}{2}\right)^2z^ndz^2\right)$ bajo $\phi$:

$$\phi^*\! \left(\left(\frac{n+2}{2}\right)^2z^ndz^2\right)=\left(\frac{n+2}{2}\right)z^nh^{\frac{2n}{n+2}}(zh^{\frac 2 {n+2}})^{\prime 2}$$
por otro lado, en cualquier punto distinto del $0$ y localmente tenemos:
$$(z^{\frac{n+2}2}h)^\prime=(zh^{\frac2{n+2}})^{\frac{n+2}2\prime}=(\frac{n+2}{2})(z^\frac n2 h^\frac n {n+2})(zh^\frac n{n+2})^\prime$$
por lo que localmente
$$\phi^*\! \left(\left(\frac{n+2}{2}\right)^2z^ndz^2\right)=(z^\frac{n+2}2h)^{\prime 2}$$
de nuevo, localmente, y derivando t\'ermino a t\'ermino la serie de $h$, es f\'acil ver que $(z^\frac{n+2}2h)^\prime=z^\frac n 2 \sqrt g$, por lo que $(z^\frac{n+2}2h)^{\prime 2}=(z^\frac n 2 \sqrt g)^2=z^ng=f$.
\ftodo
 \end{proof} 
\begin{corolario} 
En el abierto en el que est\'a definido $\phi$ las trayectorias de $\omega$ est\'an dadas por la imagen de las trayectorias de $\left(\frac{n+2}{2}\right)^2z^ndz^2$ bajo $\phi$.
 \end{corolario} 

\begin{observacion} 
Notemos que si $k$ es un real positivo, entonces las trayectorias de $\theta=f(z)dz^2$ y $k\theta:=\left(kf(z)\right)dz^2$  son las mismas, pues si $\gamma$ es una curva, entonces
$$f\bigl(\gamma(t)\bigr)\!\left(\gamma(t)^\prime\right)^2>0$$ si y solo si $$kf\bigl(\gamma(t)\bigr)\!\left(\gamma(t)^\prime\right)^2>0$$
 \end{observacion} 
\begin{ejemplo}
Veamos las trayectorias de $z^ndz^2$ para $n=0,1,\dots,5$.

\begin{figure}[h]
\centering
\subbottom[$\frac1zdz^2$]{\input{figuras/sectores/sector1.pgf}}
\subbottom[$dz^2$]{\input{figuras/sectores/sector2.pgf}}
\subbottom[$zdz^2$]{\input{figuras/sectores/sector3.pgf}}
\subbottom[$z^2dz^2$]{\input{figuras/sectores/sector4.pgf}}
\caption{Trayectorias horizontales de $z^ndz^2$}
\end{figure}
 \end{ejemplo} 

El ejemplo anterior es suficiente para extrapolar el comportamiento de las trayectorias de $z^ndz^2$ para $n>5$, por lo que el siguiete teorema deber\'ia ser totalmente esperado:

\begin{teorema} 
Sea $\omega=z^ndz^2$. Consideremos las regiones
$$\left\{p\biggr | \, \frac{2\pi k}{n+2}<\operatorname{arg}(p)<\frac{2\pi(k+1)}{n+2}\right\}, k=0,\dots,n+1.$$
Entonces la funci\'on $\phi(z)=z^{\frac{n+2}{2}}$ se restringe a un biholomorfismo en cada una de estas regiones donde adem\'as $\phi^*\!(dz^2)=\theta$.
 \end{teorema} 
%\begin{proof} 

 %\end{proof} 

\begin{corolario} 
Las trayectorias de $z^ndz^2$ est\'an dadas por los rayos ${\left\{\operatorname{arg}(p)=\frac{2\pi k}{n+2}\right\}}$ y en cada regi\'on, por la imagen de las rectas horizontales bajo $\phi^{-1}$.
 \end{corolario}


%\begin{secundario} 
\subsection{Clasificaci\'on local de las trayectorias de diferenciales abelianas}
De nuevo, los resultados anteriores tienen un an\'alogo en el caso de diferenciales abelianas:
\begin{proposicion}\label{clasificacion-local-regular} 
Si $p$ es un punto regular de la diferencial ${\omega=f(z)dz}$ entonces hay un biholomorfismo $\phi$ tal que
\begin{align*}
\phi^*\!\left(dz\right) &=\omega\\
\left(\phi^{-1}\right)^*\!(\omega) &=dz
\end{align*}
 \end{proposicion} 
\begin{corolario} 
En el abierto en el que est\'a definido $\phi$ las trayectorias de $\omega$ est\'an dadas por la imagen de las rectas horizontales (parametrizadas de izquierda a derecha) bajo $\phi^{-1}$.
 \end{corolario}


\missingfigure{figura de $zdz$ y $z^2dz$}


\begin{proposicion} 
Si $p$ es un punto cr\'itico de la diferencial ${\omega=f(z)dz}$ entoces existe un biholomorfismo $\phi$ tal que
$$\phi^*\!\left(\omega\right) =(n+1)z^ndz$$
donde $f(z)=(z-p)^ng(z)$ con $g$ holomorfa y $g(p)\neq0$ \ie $p$ es un cero de orden $n$ de $f$.
 \end{proposicion} 
\ptodo{reacomodar esto de tal forma que este teorema motiva el de las difs cuadr\'aticas}
\begin{proof} 
Notemos primero que $(n+1)z^ndz$ es una $1$-forma exacta: $$d(z^{n+1})=(n+1)z^ndz$$ por lo que si $\phi$ satisface $\phi^*((n+1)z^ndz)=\omega$ entonces $(\phi^{n+1}(z))^\prime(z)dz=d(\phi(z)^{n+1})=d(\phi^*(z^{n+1}))=\phi^*(d(z^{n+1}))=\omega$. Por lo que $\phi$ debe cumplir $(\phi(z)^n)^\prime=z^ng(z)$. Simb\'olicamente podr\'iamos escribir $\phi=(\int z^ng)^\frac1{n+1}$ por lo que el teorema estar\'ia demostrado si podemos justificar que esta expresi\'on realmente define un biholomorfismo.
Si expresamos a $g$ como una serie de potencias e integramos t\'ermino a t\'ermino la serie de $z^ng$, es claro que la serie que obtenemos se puede escribir como $z^{n+1}h$ donde $h$ es una funci\'on holomorfa y que no se anula en el cero. De aqu\'i concluimos que $h$ tiene ra\'iz $(n+1)$-\'esima bien definida cerca del cero y que la funci\'on $\phi(z)=zh^\frac1{n+1}$ es el holomorfismo buscado pues tiene derivada no nula en cero.
 \end{proof} 
\begin{corolario} 
En el abierto en el que est\'a definido $\phi$ las trayectorias de $\omega$ est\'an dadas por la imagen de las trayectorias de $(n+1)z^ndz$ bajo $\phi$
 \end{corolario} 


\begin{ejemplo}
Las trayectorias de  $z^ndz$ para $n=0,1,\dots,5$. 
\missingfigure{figura de $zdz$ y de $z^2dz$}
 \end{ejemplo} 
\begin{teorema} 
Sea $\omega=z^ndz$. Consideremos las regiones
$$\left\{p\biggr | \, \frac{2\pi k}{2n+2}<\operatorname{arg}(p)<\frac{2\pi(k+1)}{2n+2}\right\}, k=0,\dots,2n+1.$$
Entonces la funci\'on $\phi(z)=z^{n+1}$ se restringe a un biholomorfismo en cada una de estas regiones donde adem\'as $\phi^*\!(dz)=\omega$.
 \end{teorema} 

\subsection{Una diferencial cuadr\'atica que no es el cuadrado de ninguna diferencial abeliana}

De la clasificaci\'on local de las trayectorias de diferenciales abelianas y cuadr\'aticas, es aparente que las trayectorias de ninguna diferencial abeliana pueden tener la estructura que tienen las trayectorias de la diferencial cuadr\'atica $zdz^2$. De ser cierto este hecho, y como las trayectorias de una diferencial abeliana y su cuadrado tienen la misma estructura, podr\'iamos concluir que la diferencial cuadr\'atica $zdz^2$ no tiene <<ra\'iz cuadrada>>, es decir, que no existe ninguna diferencial abeliana tal que su cuadrado sea $zdz^2$.

Demostremos formalmente lo antes mencionado:

\begin{proposicion}\label{cuadratica-no-abel}
Sea $\theta=zdz^2$, $\omega$ cualquier diferencial abeliana definida sobre alg\'un abierto del plano complejo y sean $\mathcal{U}$ y $\mathcal{V}$ vecindades del $0$ y de alg\'un punto $p$ en el dominio de $\omega$. Entonces no existe ning\'un biholomorfismo $\phi$ de $\mathcal{U}$ a $\mathcal{V}$ tal que la imagen de las trayectorias horizontales de $\theta$ en $\mathcal{U}$ sean trayectorias horizontales de $\omega$ en $\mathcal{V}$.
 \end{proposicion}
\begin{proof}

Componiendo con una traslaci\'on apropiada, podemos asumir que $\phi(0)=p$ y trabajando directamente con $\phi^*(\omega)$ podemos asumir que el biholomorfismo de hecho es la identidad.

Sea $\varepsilon>0$ tal que $\varepsilon \mathbb{S}^1:=\{\varepsilon e^{i\pi t}| t\in \mathbb{R}\}\subseteq \mathcal{U}\cap\mathcal{V}\cap\{x\in \mathcal{V}| x \text{ no es cero de }\omega\}$. Si $x\in\varepsilon\mathbb{S}^1$ y $\gamma$ es cualquier trayectoria horizontal de $\theta$ que pasa por $x$ en el tiempo $t$ (\ie $\gamma(t)=x$) entonces $w:=D_t\gamma$ satisface:
$$\mathbb{R}\ni xw^2>0$$
lo cual nos lleva a considerar el subconjunto del plano complejo de las soluciones de esta desigualdad, as\'i, sea $f(x):=\{w\in \mathbb{C}| \mathbb{R}\ni xw^2>0\}$. Es claro que este conjunto es de la forma $\mathbb{R}^*v:=\{tv|0\neq t\in\mathbb{R}\}$ para alg\'un $v\in\mathbb{C}$ no nulo, a saber, $v=\pm\sqrt{\overline x}$. En otras palabras, podemos pensar que $f(x)$ es un elemento de $\mathbb{R}\mathbb{P}^1$, es decir, hemos definido una funci\'on
$$f:\varepsilon\mathbb{S}^1\rightarrow \mathbb{R}\mathbb{P}^1$$
$$x\mapsto [\pm\sqrt{\bar x} ]$$
 es f\'acil ver que esta funci\'on es de hecho una biyecci\'on.

Supongamos que $\omega=h(z)dz^2$, un razonamiento totalmente an\'alogo al anterior nos lleva a considerar la funci\'on:
$$g:\varepsilon\mathbb{S}^1\rightarrow \mathbb{RP}^1$$
$$x\mapsto [\pm\overline{h(x)}]$$
donde ahora si $\gamma$ es cualquier trayectoria horizontal de $\omega$ que pasa por $x$ en el tiempo $t$, entonces $D_t\gamma=g(x)$.

Si suponemos que las trayectorias horizontales de $\theta$ son exactamente las de $\omega$, entonces debemos concluir que $f=g$.
Sin embargo, es claro que la funci\'on $g$ se factoriza de la forma $p\circ\tilde{g}$ donde $p:\mathbb{S}^1\rightarrow\mathbb{RP}^1$ es el cociente usual, y $\tilde{g}(x)=\frac{\overline{h(x)}}{\|h(x)\|}$\attention por lo que no es una biyecci\'on, de hecho, si $\tilde{g}$ no es inyectiva, entonces $g$ tampoco puede serlo, pero si $\tilde{g}$ fuese inyectiva, tendr\'ia que ser suprayectiva (toda funci\'on continua e inyectiva de $\mathbb{S}^1$ en $\mathbb{S}^1$ es suprayectiva) y como cualquier punto y su ant\'ipoda tienen la misma imagen bajo $p$, $g$ no puede ser inyectiva.\end{proof}

%\begin{observacion} 
%la diferencia entre $zdz$ y $zdz^2$ y intuitivamente la de la \'ultima no corresponde a ning\'un punto singular de alguna dif abeliana.\todo{explicar m\'as}
 %\end{observacion} 

%\begin{proposicion} 
%no hay an\'alogo a $zdz^2$ en dif abelianas
 %\end{proposicion} 

%Por la proposici\'on anterior vemos que las diferenciales cuadr\'aticas realmente son una extensi\'on no trivial del concepto de diferencial abeliana, y que esto est\'a relacionado con la existencia de <<campos de direcciones>> que no se pueden ver como generados por campos vectoriales

 %\end{secundario} 

\begin{figure}[h]
\centering
\subbottom[$z^{-3}dz^2$]{\input{figuras/sectores/n1.pgf}}
\subbottom[$z^{-4}dz^2$]{\input{figuras/sectores/n2.pgf}}
\subbottom[$z^{-5}dz^2$]{\input{figuras/sectores/n3.pgf}}
\subbottom[$z^{-6}dz^2$]{\input{figuras/sectores/n4.pgf}}
\caption{Trayectorias horizontales de $z^{-n}dz^2$}
\end{figure}




\section{Diferenciales cuadr\'aticas meromorfas}

\begin{definicion} 
Una \emph{diferencial cuadr\'atica meromorfa} sobre $\mathcal{U}$ con polos $x_1,x_2,\dots,x_k\in \mathcal{U}$ es una expresi\'on de la forma
$$f(z)dz^2$$
donde $\phi(z)$ es una funci\'on meromorfa sobre el abierto $\mathcal{U}$ con polos $x_1,x_2,\dots,x_k$.
 \end{definicion} 


Es claro que la ecuaci\'on que determina las trayectorias de una diferencial cuadr\'atica meromorfa no tiene sentido en los polos de la diferencial, pero en el abierto $\mathcal{U}\setminus \{x_1,x_2,\dots,x_k\}$ la diferencial se restringe a una diferencial cuadr\'atica holomorfa y ah\'i podemos hablar de las trayectorias horiontales.

La definici\'on de punto regular y cr\'itico es la misma para diferenciales meromorfas que para diferenciales cuadr\'aticas, sin embargo, en el caso de las meromorfas, los ceros ya no son los \'unicos puntos cr\'iticos, pues los polos tampoco son puntos regulares.

La clasificaci\'on local de las trayectorias de una diferencial meromorfa es exactamente igual al caso holomorfo en todos los ceros y puntos regulares, por lo que basta clasificar el comportamiento al rededor de los polos para tener la clasificaci\'on completa.


\begin{proposicion} 
Sea $\theta=f(z)dz^2$ una diferencial meromorfa y $p$ un polo de orden impar de esta. Entonces existe un biholomorfismo $\phi$ tal que
$$\phi^*\!\left(\omega\right) =\left(\frac{2+n}{2}\right)^2z^{n}dz^2$$
donde $-n$ es el orden de $p$.
 \end{proposicion} 
\begin{proof} 
Un an\'alisis detallado de la demostraci\'on de la proposici\'on \ref{prop-clasif-ceros-dq} nos muestra que el mismo m\'etodo funciona en este caso. Lo cierto es que el \'unico punto delicado de la demostraci\'on era la construcci\'on de la funci\'on $h$, la cual construimos como una serie de potencias de la forma
$$\sum_{i\geq0} \frac{a_i}{\frac n2 +i+1}z^i$$
de donde es claro que no necesitamos que $n$ sea positivo, basta que no sea par.
\end{proof} 
\begin{corolario} 
En el abierto en el que est\'a definido $\phi$ las trayectorias de $\omega$ est\'an dadas por la imagen de las trayectorias de $\left(\frac{n+2}{2}\right)^2z^ndz^2$ bajo $\phi$.
 \end{corolario} 

El caso en que la diferencial cuadr\'atica tenga un polo de orden $-2$ es diferente a los dem\'as casos y lo trataremos individualmente:

\begin{proposicion}
Sea $\theta$ la diferencial cuadr\'atica $\frac{g(z)}{z^2}dz^2$ donde $g$ es una funci\'on holomorfa y que satisface $g(0)\neq0$. Entonces existe un biholomorfismo $\phi$ definido en una vecindad del cero tal que $\phi^*(\frac{g(0)}{z^2}dz^2)=\theta$.
 \end{proposicion}
\begin{proof}
Como $g$ no se anula en el cero, podemos encontrar una ra\'iz cuadrada holomorfa bien definida \ie sea $h$ una funci\'on holomorfa tal que $h(z)^2=g(z)$. 
Podemos escribir a $h$ de la forma $h(z)=h(0)+z\alpha(z)$ donde $\alpha$ es una funci\'on holomorfa. Sea $\beta$ una funci\'on holomorfa tal que $\beta^\prime=\alpha/h(0)$. Finalmente, sea $\phi=ze^\beta$.

Un c\'alculo sencillo muestra que efectivamente $\phi^*(\frac{g(0)}{z^2}dz^2)=\theta$.
 \end{proof}
\ftodo

Notemos que en este caso no fue posible transformar a $\theta$ a una diferencial de la forma $\frac{dz^2}{z^2}$ por lo que el comportamiento de las trayectorias horizontales de $\theta$ puede depender del par\'ametro $g(0)$.

El caso que a\'un no hemos consderado es que la diferencial cuadr\'atica tenga un polo de orden par mayor o igual a $4$. En este caso tampoco podemos llevar la diferencial a la forma $\frac{dz^2}{z^2m}$ con $m>1$, pero esto realmente no afecta el estudio de las trayectorias horizontales cerca del polo.

\begin{proposicion} 
Si $\theta$ tiene un polo de orden $2m$ con $m\geq 2$ entonces existe un biholomorfismo $\phi$ tal que $\phi^*(\theta)=(z^{-m} +bz^{-1})^2dz^2$.
 \end{proposicion}
\ftodo



\section[El comportamiento al infinito de una diferencial meromorfa: Diferenciales cuadr\'aticas sobre la esfera de Riemann][Diferenciales cuadr\'aticas sobre la esfera \dots]{El comportamiento al infinito de una diferencial meromorfa: Diferenciales cuadr\'aticas sobre la esfera de Riemann}

De la misma forma en que hemos estudiado el comportamiento de diferenciales cuadr\'aticas en la cercan\'ia de puntos regulares y cr\'iticos, es interesante saber cu\'al es el comportamiento al infinito (por ejemplo en el complemento de un conjunto compacto arbitrario) para lo cual, resultan muy \'utiles las mismas t\'ecnicas empleadas en la clasificaci\'on de los puntos cr\'iticos, sobre todo el uso de transformaciones de la diferencial.

Por otro lado, el biholomorfismo m\'as evidente que intercambia el cero con el <<punto al infinito>> es $\phi(z):=1/z$, por lo que de manera intuitiva podr\'iamos decir que si $\omega$ es una diferencial cuadr\'atica sobre $\mathbb{C}$, entonces el comportamiento al infinito de $\omega$ es el comportamiento cerca del $0$ de $\phi^*(\omega)$

\begin{ejemplo} 
Sea $\omega=z^ndz^2$. Entonces $\phi^*(\omega)=z^{-n-4}dz^2$ por lo que las trayectorias cerca del infinito est\'an dadas por las trayectorias cerca del cero de $z^{-n-4}dz^2$.
 \end{ejemplo} 
\ptodo{completar y dibujo}

Para poder entender geom\'etricamente y formalizar la idea anterior, consideremos la construcci\'on usual de la esfera de Riemann: $\hat{\mathbb{C}}=\mathbb{C}\cup \{\infty\}$:

\todo{mejorar todo esto}
Sea $\mathbb{S}^2$ la esfera unitaria en $\mathbb{R}^3$, y sea $p$ la proyecci\'on estereogr\'afica desde el polo norte $N=(0,0,1)$. Entonces $p$ es un homeomorfismo entre $\mathbb{C}$ y $\mathbb{S}^2\setminus\{N\}$
entonces, usando $p$ podemos transportar las trayectorias de la diferencial $\omega$ del plano complejo a $\mathbb{S}^2\setminus\{N\}$. Adem\'as, notemos que el polo norte de la esfera juega el papel del punto al infinito $\infty$ pues sus vecindades abiertas corresponden bajo $p$ a los complementos de conjuntos compactos del plano complejo\footnote{En otras palabras la esfera junto con la transformaci\'on $p^{-1}$ son la compactaci\'on por un punto del plano.}; por lo que conocer las trayectorias cerca de este punto nos permitir\'ia conocer las trayectorias <<al infinito>> en el plano. Desafortunadamente, no hay una noci\'on de diferencial cuadr\'atica sobre la esfera, pues no es un subconjunto del plano complejo, por lo que no tiene sentido considerar $p^*(\omega)$ (si esto tuviese sentido, y usando la clasificaci\'on local de las trayectorias de diferenciales cuadr\'aticas, obtendr\'iamos la respuesta a nuestra interrogante). 

Ahora, as\'i como se puede considerar la proyecci\'on estereogr\'afica desde el polo norte $N$, podemos considerar la proyecci\'on desde el polo sur $S=(0,0,-1)$, que ahora est\'a definida en el conjunto ${\mathbb{S}^2\setminus\{S\}}$ \ie est\'a bien definida cerca del polo norte. De manera an\'aloga podemos transportar las trayectorias cerca de $N$ a trayectorias cerca del $0$ en el plano complejo.

En resumidas cuentas, combinando ambas ideas, podemos transportar las trayectorias de $\omega$ a la esfera y luego al plano mediante la composici\'on $\tau \circ p_S\circ p_N$ (donde $\tau$ es la conjugaci\'on compleja) pero ahora esta funci\'on es una transformaci\'on holomorfa de $\mathbb{C}\setminus\{0\}$ en s\'i mismo y est\'a dada por $z\mapsto 1/z$\,\footnote{Esto es lo que justifica haber usado este biholomorfismo anteriormente.} por lo que en este caso s\'i se puede hablar de la imagen transpuesta \pullBack de $\omega$; es decir, hemos demostrado
\begin{proposicion} 
Sea $\omega=f(z)dz^2$ una diferencial cuadr\'atica sobre $\mathbb{C}$, si transportamos las trayectorias de $\omega$ a $\mathbb{S}$ bajo $p_N$ entonces las trayectorias cerca de $N$ est\'an dadas por la imagen de las trayectorias cercanas al $0$ de $\phi^*(\omega)=\frac{-f(1/z)}{2z^2}dz^2$ bajo $p_S \circ \tau$
 \end{proposicion} 
\begin{corolario} 
Si dos diferenciales $\omega$ y $\eta$ (sobre $\mathbb{C}$) est\'an relacionadas bajo $\phi^*(\omega)=\eta$ (en $\mathbb{C}\setminus\{0\}$) entonces al transportar las trayectorias de $\omega$ bajo $p_n$, estas coinciden con las trayectorias de $\eta$ transportadas bajo $p_s$
 \end{corolario} 
%\begin{ejemplo}
%Las trayectorias de una diferencial en la esfera de Riemann\\ 
%\missingfigure{trayectorias en la esfera de riemann}
 %\end{ejemplo} 


\begin{observacion} 
Si fuese posible hablar de diferenciales cuadr\'aticas sobre la esfera $\mathbb{S}^2$ el argumento hubiese sido mucho m\'as simple y conciso. En gran medida, el prop\'osito del cap\'itulo siguiente es desarrollar los conceptos necesarios para que tenga sentido hablar de diferenciales cuadr\'aticas sobre $\mathbb{S}^2$.
 \end{observacion} 
\section*{Notas del cap\'itulo}
El estudio y clasificaci\'on local de las trayectorias de diferenciales cuadr\'aticas se pueden consultar en \cite{strebel-quadratic}.
