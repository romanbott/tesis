%! TEX root = master.tex
\chapter{Nociones de an\'alisis complejo}
En este cap\'itulo formularemos las nociones b\'asicas de an\'alisis complejo en el lenguaje de \emph{derivadas de G\^ateaux}, \emph{formas diferenciales} y de \emph{simplejos singulares lisos}. Si bien todos los resultados que mencionamos son de un car\'acter cl\'asico, la formulaci\'on que presentamos es relativamente moderna\footnote{Muchas ideas ya se encontraban en los trabajos de Poincar\'e, pero fue \'Elie Cartan quien formaliz\'o y explot\'o las ideas concernientes al \'algebra de formas diferenciales; ve\'ase \cite{whitehead1952elie} para una descripci\'on de las aportaciones de Cartan.} y ser\'a ampliamente usada en los cap\'itulos posteriores. 

\section[Derivadas de G\^ateaux y ecuaciones de Cauchy-Riemann][Derivadas de G\^ateaux \dots]{Derivadas de G\^ateaux y ecuaciones de Cauchy-Riemann}
Comencemos estableciendo las convenciones de diferenciabilidad que usaremos:

En lo sucesivo $V$ y $W$ denotar\'an dos espacios vectoriales (reales)  de dimensi\'on finita. Usaremos adem\'as que estos espacios tienen una topolog\'ia natural que los hace espacios vectoriales topol\'ogicos;\footnote{Hay muchas maneras de construir dicha topolog\'ia; posiblemente la m\'as f\'acil sea usar un isomorfismo (lineal) de $V$ con alg\'un espacio euclidiano $\mathbb{R}^n$ para copiar la topolog\'ia del mismo. Esta topolog\'ia no depende del isomorfismo utilizado pues todo automorfismo lineal de $\mathbb{R}^n$ es un homeomorfismo.} los ejemplos que el lector debe tener en mente todo el tiempo son los espacios euclidianos $\mathbb{R}^n$ y el plano de los n\'umeros complejos $\mathbb{C}$.


Sea $f:\mathcal{U}\rightarrow W$ una funci\'on definida en un abierto $\mathcal{U}\subseteq V$, $p\in\mathcal{U}$, y $v\in V$.

\begin{definicion}
Decimos que $f$ es \emph{G\^ateaux derivable} en la direcci\'on $v$ en el punto $p$ si el siguiente l\'imite\tablefootnote{Aqu\'i $h$ es un n\'umero real no nulo, y la expresi\'on est\'a bien definida gracias a las condiciones que exigimos a $V$ y $W$; de nuevo, exhortamos al lector a tener en mente los ejemplos ya mencionados.} existe:
$$\lim_{h\rightarrow 0} \frac{f(p+hv)-f(p)}{h}$$
en tal caso, denotamos dicho l\'imite como $\partial_vf(p)$.
 \end{definicion}

\begin{ejemplo}
Si $\mathcal{U}$ es un abierto de $\mathbb{R}^n$ y $f:\mathcal{U}\rightarrow \mathbb{R}$, entonces $\partial_{e_i}f(p)$ es la derivada parcial con respecto a $x_i$ de $f$ en $p$, donde $e_i$ es el $i$-\'esimo elemento de la base natural de $\mathbb{R}^n$; la derivada parcial usualmente se denota $\frac{\partial f}{\partial x_i}(p)$.
 \end{ejemplo}

Es f\'acil ver que la derivada de G\^ateaux satisface las siguientes propiedades:
$$\partial_{\alpha v}f=\alpha \partial_v f$$
$$\partial_v (f+\lambda g)=\partial_v f + \lambda \partial_vg$$
donde $\alpha,\lambda\in \mathbb{R}$ y $f$ y $g$ son derivables en la direcci\'on de $v$.

Si $f$ es derivable en $p$ en cualquier direcci\'on, podemos definir la funci\'on:
\begin{align*}D_pf:V&\longrightarrow W\\
v&\mapsto \partial_v f(p).
\end{align*}
Si $D_pf$ es lineal, diremos que $f$ es \emph{derivable} en $p$. As\'i mismo, diremos que $f$ es \emph{derivable} (o de clase $C^1$) si es derivable en todo punto de su dominio y adem\'as, la funci\'on 
\begin{align*}
Df:\mathcal{U}&\longrightarrow L(V,W)\\
p&\mapsto D_pf
\end{align*}
es continua.\footnote{En este caso, impl\'icitamente estamos asumiendo que podemos dotar a $L(V,W)$ de una topolog\'ia <<razonable>>. Una manera de obtener dicha topolog\'ia es usar el hecho de que $L(V,W)$ es isomorfo a $\mathbb{R}^{(\dimension(V)\dimension(W))}$ y usar el procedimiento antes descrito para dotar de una topolog\'ia a los espacios vectoriales de dimensi\'on finita.}
A la funci\'on anterior le llamaremos la \emph{derivada} de $f$.

Finalmente, definimos de manera recursiva las funciones de \emph{clase $C^n$} como aquellas que son derivables y cuya derivada es de clase $C^{n-1}$. Si una funci\'on es de clase $C^n$ para todo entero positivo $n$, diremos que es \emph{lisa}.

\begin{observacion}La base $\{1\}$ de $\mathbb{R}$ da lugar a una identificaci\'on  entre $L(\mathbb{R},\mathbb{R})$ y $\mathbb{R}$. Usando esta identificaci\'on, es f\'acil ver que la derivada de una funci\'on $f:\mathcal{U}\rightarrow \mathbb{R}$ con $\mathcal{U}$ un abierto de $\mathbb{R}$ coincide con la derivada usual de funciones reales.
 \end{observacion}

Cada punto \( (x,y)\in \mathbb{R}^2\) tiene un n\'umero complejo asociado \( z(x,y):=x+iy\).
Inversamente, todo n\'umero  complejo \( z\) tiene una pareja de n\'umeros reales asociados, 
las partes real e imaginaria de $z$,
$\Re(z)$ y $\Im(z)$.
Es claro que las funciones $z$ y $(\Re,\Im)$ son inversas y por lo general las usaremos para identificar a $\mathbb{R}^2$ con el campo de los n\'umeros complejos $\mathbb{C}$.


%Si $(x,y)$ es un punto de $\mathbb{R}^2\!$, podemos asociarle el n\'umero complejo ${z(x,y):=x+iy}$,
%e inversamente, si $z$ es un n\'umero complejo, podemos asociarle una pareja de n\'umeros reales $\Re(z)$ y $\Im(z)$, las partes real e imaginaria de $z$, respectivamente. Es claro que las funciones $z$ y $(\Re,\Im)$ son inversas y por lo general las usaremos para identificar a $\mathbb{R}^2$ con el campo de los n\'umeros complejos $\mathbb{C}$.

Por ejemplo, si $f:(a,b)\rightarrow\mathbb{C}$ es una curva (parametrizada) en el plano complejo, entonces es derivable si y \solo{} si las dos curvas reales $\Re(f)$ y $\Im(f)$ lo son y en tal caso $Df= D\Re(f) + iD\Im(f)$.

A partir de ahora (y en general en todo el trabajo) las funciones en cuesti\'on son funciones que toman valores en el campo de los n\'umeros complejos; en particular usaremos la notaci\'on $C^\infty(\mathcal{U})$ para denotar el \'algebra de funciones lisas con valores complejos definidas sobre $\mathcal{U}$.

\begin{definicion} 
Una funci\'on $f:\mathcal{U}\rightarrow\mathbb{C}$ definida sobre un abierto $\mathcal{U}\subseteq \mathbb{C}$ es \emph{holomorfa en $\mathcal{U}$} si para todo punto $p\in\mathcal{U}$ el l\'imite
$$\lim_{z\rightarrow p} \frac{f(z)-f(p)}{z-p}$$
existe. En este caso dicho l\'imite recibe el nombre de \emph{la derivada de $f$ en $p$} y se denota $f^\prime(p)$.
 \end{definicion} 

%A pesar de que la definci\'on de funci\'on holomorfa evidentemente es el an\'alogo complejo de la noci\'on de funci\'on derivable real, estos dos conceptos difieren enormemente. Algunos resultados exclusivos del an\'alisis complejo son:

\begin{definicion} 
Si $f$ es una funci\'on holomorfa entonces definimos recursivamente las \emph{derivadas superiores} de $f$ de acuerdo a:
\begin{align*}
f^0 & = f\\
f^{n+1} & = (f^n)^\prime
\end{align*}
 \end{definicion} 

Uno de los resultados m\'as importantes del an\'alisis complejo es el siguiente teorema:
\begin{teorema} 
 Si $f:\mathcal{U}\rightarrow\mathbb{C}$ es holomorfa en $\mathcal{U}$ entonces la funci\'on:
$$z\mapsto f^\prime(z)$$
es holomorfa tambi\'en.
 \end{teorema} 
Este teorema garantiza la existencia de todas las derivadas superiores de una funci\'on holomorfa y, en cierto modo, marca una diferencia fundamental entre el an\'alisis real y el complejo.

\begin{ejemplo} 
La funci\'on $f(z)=1/z$ es holomorfa en $\mathbb{C}\setminus \{0\}$ pues si $z_0\neq0$ entonces para cualquier $z\neq z_0$ se tiene que:
$$\frac{f(z)-f(z_0)}{z-z_0}=\frac{1/z-1/z_0}{z-z_0}=\frac{\frac{z_0-z}{z_z0}}{z-z_0}=-\frac1{zz_0}$$
por lo que
$$\lim_{z\rightarrow z_0}\frac{f(z)-f(z_0)}{z-z_0}=\lim_{z\rightarrow z_0}-\frac1{zz_0}=-\frac1{z^2}$$
%$$\frac{f(z+h)-f(z)}{h}=\frac{1/(z+h)-1/z}{h}=\frac{z-(z+h)}{h(z+h)(z)}=-\frac1{z^2+hz}$$
%por lo que
%$$\lim_{h\rightarrow 0}\frac{f(z+h)-f(z)}{h}=\lim_{h\rightarrow 0}-\frac1{z^2+zh}=-\frac{1}{z^2}$$
\ie $f^\prime(z)=-\frac{1}{z^2}$.
 \end{ejemplo} 



\begin{teorema}[ecuaciones de Cauchy-Riemann]
Si $f$ es una funci\'on holomorfa, entonces
$$f^\prime(z)=\frac{\partial f}{\partial x}(z)=-i\frac{\partial f}{\partial y}(z)$$
 \end{teorema}



%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
%>>>>>
%>>>>>>>>>>>>>>>>>>>	FORMAS DIF COMPLEJAS	<<<<<<<<<<<<<<<<<<<<<<<
%>>>>>
%>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>

\section{Formas diferenciales complejas}

Podr\'ia decirse que, a grandes razgos, las $n$-formas diferenciales son las cantidades que se pueden integrar sobre regiones $n$-dimensionales; \'estas constituyen un marco en el que expresiones del estilo
$\int_D f dx\, dy$ tienen sentido y adem\'as permite extender nociones de c\'alculo al contexto de variedades lisas, lo cual resultar\'a fundamental cuando m\'as adelante trabajemos sobre superficies lisas arbitrarias y no s\'olo sobre abiertos del plano complejo. Es por esto que en este cap\'itulo, desarrollamos de manera general la teor\'ia b\'asica de formas diferenciales e integraci\'on sobre espacios euclidianos, aunque teniendo siempre en mente los casos de dimensi\'on 1 y 2, por ser estos dos casos los que corresponden a integrar en $\mathbb{R}$ y $\mathbb{C}$ y que posteriormente corresponder\'an a integrar sobre curvas y superficies.

\begin{definicion}
El \emph{\'algebra de Grassmann} en $n$ generadores $\Omega_n$ es la $\mathbb{C}-$\'algebra graduada generada por $n$ elementos homog\'eneos de grado uno: $dx_1,dx_2,\dots,dx_n$  que satisfacen las siguentes relaciones:
\begin{align*}
dx_i^2 & =0,\\
dx_idx_j & =-dx_jdx_i.
\end{align*}
 \end{definicion}

\begin{observacion}
En el caso $n=1$ el \'unico generador $dx_1$ lo escrbiremos como $dt$; an\'alogamente, en el caso $n=2$ los \'unicos dos generadores $dx_1$ y $dx_2$ los escribiremos como $dx$ y $dy$.
 \end{observacion}

\begin{definicion}
El \'algebra de \emph{formas diferenciales} sobre $\mathbb{R}^n$ es el \'algebra graduada
$$\Omega(\mathbb{R}^n):=C^\infty(\mathbb{R}^n)\otimes_\mathbb{C} \Omega_n.$$
 \end{definicion}




\begin{observacion}
Recordemos que
$$C^\infty(\mathbb{R}^n):=\{f:\mathbb{R}^n\rightarrow\mathbb{C}|~f \text{ es lisa}\}$$
es una $\mathbb{C}-$\'algebra, por lo que, en la definici\'on anterior, tiene sentido considerar el producto tensorial sobre los complejos. Por otro lado, $\Omega(\mathbb{R}^n)$ es naturalmente un $C^\infty(\mathbb{R}^n)$-m\'odulo. La graduaci\'on es la inducida por la graduaci\'on de $\Omega_n$ y adem\'as, cualquier conjunto de generadores $\{b_i\}$ de $\Omega_n$ como $\mathbb{C}$ espacio vectorial induce un conjunto de generadores de $\Omega(\mathbb{R}^n)$ de la forma $\{1\otimes b_i\}$%
%\footnote{Esta es la \'unica ocasion en que escribiremos el producto de formas usando el s\'imbolo $\otimes$, en general, solo lo escribiremos como la concatenaci\'on de las formas.} (como $C^\infty(\mathbb{R}^n)$ \'algebra)
.

Siguiendo la convenci\'on usual, omitiremos el s\'imbolo $\otimes$ en expresiones de la forma $f\otimes \omega$ donde $\omega$ es un elemento de $\Omega_n$ y adem\'as, el producto en $\Omega(\mathbb{R}^n)$ y en $\Omega$ lo denotaremos por $\wedge$, es decir, reemplazaremos la expresi\'on $f\otimes (dx_{i_1} dx_{i_2}\cdots dx_{i_k})$ por $fdx_{i_1}\wedge dx_{i_2}\wedge\dots\wedge dx_{i_k}$.

Por las relaciones impuestas en los generadores de $\Omega_n$ (como \'algebra) se puede ver f\'acilmente que una base para $\Omega_n$ como espacio vectorial est\'a dada por todos los elementos de la forma:
$$ dx_{i_1}\wedge dx_{i_2}\wedge\dots\wedge dx_{i_k}$$
donde $k\leq n$ e $i_j<i_{j+1}$.
Si descomponemos a $\Omega(\mathbb{R}^n)$ como
$$\Omega(\mathbb{R}^n)=\bigoplus_{k\geq 0} \Omega^k(\mathbb{R}^n)$$
donde $\Omega^k(\mathbb{R}^n)$ es la $k$-\'esima componente homog\'enea, tenemos que
$$\Omega^0(\mathbb{R}^n)\cong C^\infty(\mathbb{R}^n)$$
y que $\Omega^k(\mathbb{R}^n)$ est\'a generada por el conjunto $$\{dx_{i_1}\wedge dx_{i_2}\wedge\cdots\wedge dx_{i_k} | 1\leq i_1<i_2<\dots<i_{k-1}<i_k\leq n\}$$ y en particular concluimos que ${\Omega^k(\mathbb{R}^n)=0}$ si $n<k$. A los elementos de $\Omega^k(\mathbb{R}^n)$ se les llama $k$-formas sobre $\mathbb{R}^n$.
 \end{observacion}

\begin{ejemplo}[El caso $n=1$]
En este caso una base como espacio vectorial para $\Omega_1$ est\'a dada por $\{1,dt\}$, por lo que todo elemento de $\Omega(\mathbb{R})$  es de la forma $f+gdt$, donde $f$ y $g$ son funciones lisas.
 \end{ejemplo}

\begin{ejemplo}[El caso $n=2$]
En este caso una base como espacio vectorial para $\Omega_2$ est\'a dada por $\{1,dx,dy,dx\wedge dy\}$ y todo elemento de $\Omega(\mathbb{R}^2)$ es de la forma $f_1+f_2dx+f_3dy+f_4dx\wedge dy$.
 \end{ejemplo}



\begin{proposicion-def} 
Existe un \'unico morfismo lineal ${d:\Omega(\mathbb{R}^n)\rightarrow\Omega(\mathbb{R}^n)}$ que satisface las siguientes propiedades:
\begin{itemize}
 \item $d(f)=\sum\frac{df}{dx_i}dx_i$ si $f\in\Omega^0(\mathbb{R}^n)$
\item $d(fdx_{i_1}\wedge dx_{i_2}\wedge \dots\wedge dx_{i_k})=d(f)\wedge dx_{i_1}\wedge dx_{i_1}\wedge\dots\wedge dx_{i_k}$ para $f\in \Omega^0(\mathbb{R}^n)$
 \end{itemize}
a este morfismo se le llama la \emph{derivada exterior} y satisface adem\'as un an\'alogo de la regla de Leibniz:
$$d(\omega\wedge \tau)= d(\omega)\wedge \tau+ (-1)^{\grad\omega}\wedge\omega d(\tau)$$
 \end{proposicion-def}
En vez de dar una demostraci\'on, daremos un ejemplo en el que es evidente c\'omo estas propiedades determinan la derivada exterior.

\begin{ejemplo}
Sea $\omega=fdx+gdy\in\Omega^1(\mathbb{R}^2)$, entonces
\begin{align*}
d(fdx+gdy) & = d(fdx) + d(gdy)\\
& = d(f)dx + d(g)dy\\
& = \frac{\partial f}{\partial x} dx\wedge dx +\frac{\partial f}{\partial y} dy\wedge dx +\frac{\partial g}{\partial x} dx\wedge dy +\frac{\partial g}{\partial y} dy\wedge dy \\
& = -\frac{\partial f}{\partial y} dx\wedge dy +\frac{\partial g}{\partial x} dx\wedge dy = \left(\frac{\partial g}{\partial y}-\frac{\partial f}{\partial y}\right) dx\wedge dy
\end{align*}
 \end{ejemplo}



\begin{ejemplo}
Si $x_i$ denota la $i$-\'esima funci\'on coordenada de $\mathbb{R}^n$ (o la $i$-\'esima proyecci\'on sobre $\mathbb{R}$) entonces $d(x_i)=dx_i$.
 \end{ejemplo}

\begin{ejemplo}
Si $z$ denota la identificaci\'on de $\mathbb{R}^2$ con $\mathbb{C}$ (\ie ${z(x,y)=x+iy}$) entonces ${dz=dx+idy}$.
An\'alogamente $d\bar{z}=dx-idy$. Esto implica que $dx=\frac12(dz+d\bar{z})$ y  $dy=\frac{-i}2(dz-d\bar{z})$ por lo que $\{dz,d\bar{z}\}$ es tambi\'en un conjunto de generadores homog\'eneos para el \'algebra $\Omega(\mathbb{R}^2)$.
 \end{ejemplo}

\begin{proposicion} 
Si $f\in \Omega^0(\mathbb{C})$ es una funci\'on holomofa, entonces
$$d(f)=f^\prime dz.$$
Inversamente, si $df$ expresada en la base $\{dz, d\bar{z}\}$ no depende de $d\bar{z}$ (\ie $df=gdz +0d\bar{z}$) entonces $f$ es holomorfa.
 \end{proposicion}



\begin{proposicion-def} 
Si $\phi:\mathbb{R}^n\rightarrow\mathbb{R}^m$ es una transformaci\'on lisa, entonces existe  un \'unico morfismo de \'algebras graduadas ${\phi^*:\Omega(\mathbb{R}^m)\rightarrow\Omega(\mathbb{R}^n)}$ que satisface:
\begin{itemize}
 \item $\phi^*(d\omega)=d\phi^*(\omega)$
\item  $\phi^*(f)=f\circ\phi$ si $f\in\Omega^0(\mathbb{R}^m)$
 \end{itemize}
Si $\omega$ es una forma, a $\phi^*(\omega)$ le llamaremos la \emph{imagen transpuesta}\footnote{La palabra anglosajona para designar este concepto es \emph{pullback}, misma que algunos textos en espa\~nol no se toman la molestia en traducir; el t\'ermino que adoptamos en este trabajo se deriva del franc\'es \emph{image transpose\'e}, como aparece en \cite{deRham1955varietes}.} de $\omega$ bajo $\phi$.
 \end{proposicion-def}
Nuevamente, en vez de dar una demostraci\'on, preferimos ejemplificar c\'omo la imagen transpuesta de una forma\pullBack{} est\'a determinada por estas propiedades.
\begin{ejemplo}\label{ejemplo-pullback}
Sea $\omega=fdx+gdy\in\Omega^1(\mathbb{R}^2)$ y $\phi:\mathbb{R}^1\rightarrow\mathbb{R}^2$ una transformaci\'on lisa, entonces
\begin{align*}
\phi^*(\omega) & = \phi^*(fdx+gdy)\\
& = \phi^*(f)\phi^*(dx)+\phi^*(g)\phi^*(dy)\\
& = (f\circ\phi)\phi^*(d(x))+(g\circ\phi)\phi^*(d(y))\\
& = (f\circ\phi)d(\phi^*(x))+(g\circ\phi)d(\phi^*(y))\\
& = (f\circ\phi)\left (\frac{\partial \phi_1}{\partial t}dt\right )+(g\circ\phi)\left (\frac{\partial \phi_2}{\partial t}dt\right) \\
& =  \left ((f\circ\phi)\frac{\partial \phi_1}{\partial t}+ (g\circ\phi)\frac{\partial \phi_2}{\partial t}\right )dt
\end{align*}
Donde $\phi_1:=\phi^*(x)=x\circ\phi$ y $\phi_2:=\phi^*(y)=y\circ\phi$ \ie $\phi=(\phi_1,\phi_2)$.
 \end{ejemplo}

Para finalizar esta secci\'on, s\'olo nos resta mencionar que todas las construcciones anteriores siguen teniendo sentido si reemplazamos los espacios euclidianos $\mathbb{R}^n$ por abiertos de estos: si $\mathcal{U}\subseteq \mathbb{R}^n$ es un abierto, definimos el \'algebra de formas sobre $\mathcal{U}$ como 
$$\Omega(\mathcal{U}):=C^\infty(\mathcal{U})\otimes_\mathbb{C}\Omega_n.$$





\section{Integraci\'on de formas y teorema de Stokes}

En esta secci\'on desarrollamos la terminolog\'ia y resultados que nos permitir\'an hablar de la integral de una forma. Comenzamos con los conjuntos m\'as simples sobre los que se puede definir la integral de una forma.

\begin{definicion}
El $n$-simplejo est\'andar $\Delta_n$ es el m\'inimo conjunto convexo que contiene al conjunto $\{0,e_1,\dots,e_n\}$ donde $\{e_i\}$ es la base natural de $\mathbb{R}^n$.\footnote{Cabe aclarar que \'esta no es la definici\'on usual de simplejo est\'andar; usualmente se define $\Delta_n$ como el m\'inimo conjunto convexo que contiene a $\{e_1,\dots,e_n,e_{n+1}\}$ en $\mathbb{R}^{n+1}$.}

Es decir, el $0$-simplejo est\'andar es s\'olo un punto; el $1$-simplejo estandar es el intervalo $[0,1]$ y el $2$-simplejo estandar es el tri\'angulo en $\mathbb{R}^2$ con v\'ertices $(0,0)$, $(1,0)$ y $(0,1)$.
\end{definicion}



\begin{definicion}
Sea $\omega=fdx_1\wedge dx_2\wedge \cdots \wedge dx_n$ una $n$-forma sobre un abierto $\mathcal{U}\subseteq \mathbb{R}^n$ tal que $\Delta_n\subseteq \mathcal{U}$. Definimos la \emph{integral} de $\omega$ sobre $\Delta_n$ como 
$$\int_{\Delta_n}\omega:=\int_{\Delta_n}f$$

%Si $\omega=fdt\in \Omega^1(\mathcal{U})$ es una 1-forma sobre abierto de $\mathbb{R}$ tal que $\Delta_1=[0,1]\subseteq \mathcal{U}$, definimos la integral de $\omega$ sobre $\Delta_1$ como
%$$\int_{\Delta_1} \omega =\int_0^1f.$$
%An\'alogamente definimos la integral de una 2-forma $\omega=fdx\,dy\in \Omega^2(\mathcal{U})$ (con $\mathcal{U}$ abierto de $\mathbb{C}$ tal que $\Delta_2\subseteq \mathcal{U}$) sobre $\Delta_2$ como 
%$$\int_{\Delta_2} \omega=\int_{\Delta_2} f.$$
 \end{definicion}
De la definici\'on es evidente que si \( {\omega=f\ dx_1\!\wedge\! \cdots\!\wedge dx_n}\) y ${\tau=g\ dx_1\!\wedge\! \cdots\!\wedge dx_n}$ son dos formas en $\mathcal{U}$ tal que $f=g$ en $\Delta_n$ entonces $\int_{\Delta_n}\!\omega=\int_{\Delta_n}\!\!\tau$.

Una ventaja de estudiar formas diferenciales y simplejos es que, en gran medida, la teor\'ia de integraci\'on de formas diferenciales se reduce a definir la integral de una forma sobre los simplejos est\'andar.

\begin{definicion}
Sea $\gamma:\Delta_1\rightarrow \mathcal{U}$ una curva lisa y $\omega$ es una $1$-forma sobre $\mathcal{U}$. Definimos la \emph{integral de $\omega$ a lo largo de $\gamma$} como
$$\int_\gamma \omega=\int_{\Delta_1} \bar{\gamma}^*(\omega)$$
donde $\bar{\gamma}$ es cualquier extensi\'on de $\gamma$ a un abierto $\mathcal{V}$ tal que $\Delta_1\subseteq \mathcal{V}$, y $\bar{\gamma}^*(\omega)$ es la imagen transpuesta de $\omega$ bajo esta extensi\'on.
 \end{definicion}

\begin{observacion}
En la definici\'on anterior la integral no depende de la extensi\'on, pues \solo{} depende de los valores de $\bar{\gamma}$ en $\Delta_n$, y ah\'i $\bar\gamma$ tiene que ser igual a $\gamma$. Por otro lado, siempre supondremos que existe dicha extensi\'on lisa.
 \end{observacion}

En este contexto, la generalizaci\'on natural de \emph{curva lisa} a dimensiones superiores es la noci\'on de simplejo singular liso:

\begin{definicion}
Un $n$-\emph{simplejo singular} (liso) es una funci\'on continua ${s\!:\!\Delta_n\!\rightarrow\!\mathcal{U}}$ que se puede extender a una funci\'on lisa definida en una vecindad de $\Delta_n$ y donde \(\mathcal{U} \) es un abierto de \( \mathbb{R}^m\).
 \end{definicion}

De forma an\'aloga a como definimos la integral de una $1$-forma sobre una curva, podemos definir la integral de una $n$-forma sobre un $n$-simplejo singular.

\begin{definicion}
Si $\omega$ es una $n$-forma sobre $\mathcal{U}$ y $s:\Delta_n\rightarrow\mathcal{U}$ es un $n$-simplejo singular, definimos la integral de $\omega$ sobre $s$  como
$$\int_s \omega=\int_{\Delta_1}\bar{s}^*(\omega)$$
donde $\bar{s}$ es cualquier extensi\'on lisa de $s$ a una vecindad de $\Delta_n$.
 \end{definicion}
\begin{ejemplo}\label{puntos-simp}
Sea $p$ un punto en $\mathcal{U}$,$f$ una $0$-forma y denotemos por $\hat p$ al $0$-simplejo tal que $\hat p(0)=p$. Con esta notaci\'on, es claro que $\int_{\hat{p}}f=f(\hat{p}(0))=f(p)$ es decir, la evaluaci\'on de $f$ en $p$.
 \end{ejemplo}

\begin{ejemplo}
Si $f:\mathbb{C}\rightarrow\mathbb{C}$ es una funci\'on lisa y $\gamma:\Delta_1\rightarrow\mathbb{C}$ es una curva en $\mathbb{C}$, entonces $\gamma^*(dz)=d(z\circ\gamma)=D\gamma dt$ por lo que
$$\int_\gamma fdz=\int_0^1 f(\gamma(t))D\gamma dt.$$
 \end{ejemplo}

Hay dos posibles limitantes en esta forma de desarrollar la teor\'ia de integraci\'on de formas, la primera, es que puede ser necesario integrar sobre curvas  (o simplejos) cuyo dominio no es el $1$-simplejo est\'andar; la soluci\'on a este problema es utilizar difeomorfismos para <<transportar>> la definici\'on de integral a conjuntos que posiblemente no son simplejos est\'andar, pero al menos son difeomorfos a estos; m\'as formalmente tenemos:
\begin{definicion}
Sea $D$ un conjunto de $\mathbb{R}^n$ que es difeomorfo al {$n$-simplejo} est\'andar, y $\omega$ una $n$-forma definida en un abierto que contiene a $D$. Entonces, definimos la integral de $\omega$ sobre $D$ como
$$\int_D\omega:=\int_{\Delta_n}\phi^*(\omega)$$
donde $\phi:\Delta_n\rightarrow D$ es cualquier funci\'on continua que se extiende a un difeomorfismo que preserva orientaci\'on entre una vecindad de $D$ y una vecindad de $\Delta_n$.
 \end{definicion}

Para que esta definici\'on tenga sentido, es necesario que la integral no dependa del difeomorfismo usado para definirla; el siguiente teorema garantiza justo esta propiedad:

\begin{teorema}[Teorema de cambio de variable] 
Sea $\omega$ una $n$-forma sobre el {$n$-simplejo} estandar y $\phi:\Delta_n\rightarrow\Delta_n$ un difeomorfismo que preserva orientaci\'on. Entonces
$$\int_{\Delta_n}\omega= \int_{\Delta_n}\phi^*(\omega).$$
 \end{teorema}
\begin{proof}
Sea $\omega=fdx_1\wedge x_2 \wedge \dots \wedge dx_n$ una $n$-forma diferencial, entonces $\phi^*(\omega)=(f\circ\phi)\phi^*(dx_1\wedge\dots\wedge dx_n)=(f\circ\phi)d\phi_1\wedge\dots\wedge d\phi_n$ donde $\phi_i$ es la $i$-\'esima coordenada del difeomorfismo $\phi$. No es dif\'icil convencerse de que $d\phi_1\wedge \dots \wedge d\phi_n=J(\phi)dx_1\wedge \dots \wedge dx_n$ donde $J(\phi):=det(D\phi)$ es el determinante jacobiano de $\phi$, por lo que el teorema se sigue del teorema de cambio de variable usual.
 \end{proof}

La segunda limitante es que puede ser necesario integrar formas sobre simplejos que no son suaves. Por ejemplo, resulta \'util integrar $1$-formas sobre curvas que \emph{\solo{}} son suaves por pedazos.

M\'as concretamente, si $\gamma$ es una curva suave por pedazos, que est\'a definida en un intervalo $[a,b]$, \ie existe una partici\'on $\{p_i\}$ de $[a,b]$ tal que $\gamma$ es lisa en cada subintervalo $(p_i,p_{i+1})$, entonces podemos definir una colecci\'on de curvas lisas $\gamma_i$ como la restricci\'on de $\gamma$ al intervalo $(p_i,p_{i+1})$. Finalmente, si $\omega$ es una $1$-forma definida en un abierto que contiene a la imagen de $\gamma$, es claro que \emph{debemos} definir la integral de $\omega$ sobre $\gamma$ como la suma de las integrales de $\omega$ sobre las curvas $\gamma_i$ es decir:
$$\int_\gamma \omega := \sum \int_{\gamma_i}\omega.$$
Notemos que si nos permitimos escribir $\gamma=\sum \gamma_i$ entonces la definici\'on anterior \solo{} expresa el hecho de que la integral es \emph{aditiva} sobre el dominio de integraci\'on. Inspirados por estas ideas, y en un intento de formalizar la expresi\'on $\gamma=\sum \gamma_i$ definimos:

\begin{definicion}
Una $n$-\emph{cadena singular} (lisa) es una combinaci\'on lineal finita con coeficientes complejos de $n$-simplejos singulares \ie un elemento del espacio vectorial complejo $S_n(\mathcal{U})$ libremente generado por el conjunto de simplejos singulares.
Usando la identificaci\'on del ejemplo \ref{puntos-simp} de puntos con $0$-simplejos ($p\mapsto \hat{p}$) concluimos que las $0$-cadenas son simplemente combinaciones lineales de puntos de $\mathcal{U}$.
 \end{definicion}



\begin{definicion}
Sea $s=\sum\alpha_i s_i$ una $k$-cadena y $\omega$ una $k$-forma (ambos en un abierto $\mathcal{U}\subseteq \mathbb{R}^n$). Definimos la integral de $\omega$ sobre $s$ como
$$\int_s \omega = \sum \alpha_i \int_{s_i}\omega.$$
 \end{definicion}

En esta terminolog\'ia, el teorema fundamental del c\'alculo toma la siguiente forma:
\begin{teorema}[Teorema fundamental del c\'alculo]
Si $\omega=f$ es una $0$-forma sobre $\mathbb{R}$ entonces
$$\int_{\Delta_1} d(\omega) = f(1)-f(0).$$
\end{teorema}

Si pensamos que $\partial(\Delta_1):=\hat1-\hat0$ es la \emph{frontera orientada} de $\Delta_1$, entonces la f\'ormula anterior simplemente expresa la igualdad entre $\int_{\Delta_1}d(\omega)$ y $\int_{\partial(\Delta_1)}\omega$. Ser\'ia muy \'util generalizar la f\'ormula anterior a dimensiones superiores, para lo cual necesitamos definir la frontera orientada de un $n$-simplejo.


\begin{definicion}
La frontera del $0$-simplejo est\'andar $\partial(\Delta_0)$ es $0$ (es decir, el neutro aditivo del espacio vectorial $S_{-1}(\Delta_0):=\{0\}$). 

La frontera del $1$-simplejo est\'andar $\partial(\Delta_1)$ est\'a dada por la $0$-cadena $\hat{1}-\hat0$ que es un elemento de $S_0(\Delta_1)$.

La frontera del $2$-simplejo est\'andar $\Delta_2$ est\'a dada por la $1$-cadena:
$$\partial(\Delta_2):=[(0,0),(1,0)]+[(1,0),(0,1)]-[(0,0),(0,1)]$$
donde $[x,y]$ denota al $1$-simplejo singular tal que $[x,y](t)=(1-t)x+ty$ (es decir, es el segmento orientado que une $x$ con $y$).

Si bien esta definici\'on se puede extender a dimensiones superiores, no ahondaremos en este aspecto pues \solo{} estamos interesados en las dimensiones $0$, $1$ y $2$.\footnote{En general, sea $F^p_i:\Delta_{p-1}\rightarrow\Delta_p$ la funci\'on tal que $F_i^p(\sum_{j=1}^{p-1} \lambda_je_j)=\sum_{j=1}^{i-1}\lambda_j e_j + \sum_{j=i}^{p}\lambda_j e_{j+1}$ donde $\lambda_0=1-\sum_{j=1}^{p-1} \lambda_j$. Es claro que $F_i^p$ es un $p-1$-simplejo singular sobre $\Delta_p$ y se le conoce como la \emph{$i$-\'esima cara} de $\Delta_p$. De esta forma podemos definir la frontera de $\Delta_p$ como $\partial(\Delta_p)=\sum_{i=0}^p (-1)^iF_i^p$.}
 \end{definicion}
\begin{figure}
\centering
\begin{tikzpicture}[scale=2]
\draw [pattern=north west lines, pattern color=orange] (1,0) -- (0,1) -- (0,0) -- cycle;
\node at (-0.1,-0.1) {$0$};
\node at (1.1,-0.1) {$e_1$};
\node at (-0.1,1.1) {$e_2$};
\begin{scope}[xshift=1.5cm,thick]
\draw (1,0) -- (0,1) -- (0,0) -- cycle;
\node at (-0.1,-0.1) {$0$};
\node at (1.1,-0.1) {$e_1$};
\node at (-0.1,1.1) {$e_2$};
\draw [->] (0.25,0) --  node [below] {$[0,e_1]$} (.5,0);
\draw [->] (0.75,0.25) -- (.5,0.5) node [above,right=2pt] {$[e_1,e_2]$};
\draw [->] (0,0.25) -- (0,.5) node [above, left] {$[0,e_2]$};
\end{scope}
\end{tikzpicture}
\caption{El $2$-simplejo est\'andar y las distintas <<caras>> que conforman su frontera.}
\end{figure}


Antes de extender la noci\'on de frontera a simplejos singulares y cadenas, nos ser\'a muy \'util describir una manera de transportar cadenas bajo transformaciones lisas:

\begin{definicion}
Si $\phi:\mathbb{R}^n\rightarrow\mathbb{R}^m$ es una transformaci\'on lisa, definimos el morfismo $\phi_*:S_k(\mathbb{R}^n)\rightarrow S_k(\mathbb{R}^m)$ de la siguiente forma:
$$\phi_*\left (\sum \alpha_i s_i\right )=\sum \alpha_i (\phi\circ s_i).$$
Si $C$ es una cadena, a $\phi_*(C)$ se le llama la \emph{imagen directa} de $C$ bajo $\phi$.
 \end{definicion}


\begin{definicion}
El morfismo \emph{frontera} $\partial:S_n(\mathcal{U})\rightarrow S_{n-1}(\mathcal{U})$ est\'a dado por
$$\partial\left (\sum \alpha_i s_i \right ) = \sum \alpha_i {s_i}_*(\partial(\Delta_n)).$$
(Por convenci\'on asumimos que $S_{-1}(\mathcal{U})=\{0\}$)
 \end{definicion}
\begin{observacion}
N\'otese que definir de esta forma el morfismo frontera es necesario si se quiere que se satisfaga la propiedad $\phi_*\circ \partial = \partial \circ \phi_*$, y adem\'as esta propiedad se sigue directamente de la definici\'on del morfismo frontera.
\end{observacion}

As\'i, la generalizaci\'on del teorema fundamental del c\'alculo toma la forma:

\begin{teorema}[Teorema de Stokes]
Sean $C$ una k-cadena y $\omega$ una $(k-1)$-forma, ambos sobre $\mathcal{U}$. Entonces
$$\int_{\partial C}\omega=\int_Cd\omega.$$
 \end{teorema}
%\begin{proof}
%\ptodo{revisar redaccion y prueba}
%Solo demostraremos el caso en que $k=2$, adem\'as, usando la linealidad de la integral sobre el integrando y el dominio de integraci\'on, podemos reducirnos al caso de que $C=\Delta_2$ y $\omega=fdx+gdy$. Por un lado tenemos:
%$$\int_{\Delta_2}d( fdx+gdy )=\int_{\Delta_2}d(fdx)+\int_{\Delta_2}d(gdy)$$
%$$-\int_{\Delta_2}\frac{\partial f}{\partial y}dx\,dy +\int_{\Delta_2}\frac{\partial g}{\partial x}dx\,dy$$
%Usando el teorema de Fubini podemos reescribir estas dos \'ultimas integrales en t\'erminos de integrales iteradas:
%$$-\int_{\Delta_2}\frac{\partial f}{\partial y}dx\,dy=-\int\left(\int \frac{\partial f}{\partial y}dy\right)dx=\int_0^1 f(x,0)-f(x,1-x)$$
%donde la \'ultima igualdad se debe al teorema fundamental del c\'alculo; an\'alogamente tenemos
%$$\int_{\Delta_2}\frac{\partial g}{\partial x}dx\,dy=\int g(1-y,y)-g(0,y)$$
%Por otro lado, denotemos como $\gamma_1$, $\gamma_2$ y $\gamma_3$ los segmentos orientados $[(0,0),(1,0)]$, $[(1,0),(0,1)]$ y $[(1,0),(0,0)]$ es decir, $\partial(\Delta_2)=\gamma_1+\gamma_2+\gamma_3$. Con esta notaci\'on tenemos
%$$\int_{\partial(\Delta_2)}fdx+gdy=\int_{\gamma_1}fdx+gdy + \int_{\gamma_2}fdx+gdy+\int_{\gamma_3}fdx+gdy$$
%usando el c\'alculo del ejemplo \ref{ejemplo-pullback} vemos que 
%$$\int_{\gamma_1}fdx+gdy=\int_{\gamma_1}fdx+\int_{\gamma_1}gdy=\int_{\Delta_1}\gamma_1^*(fdx)+\int_{\Delta_1}\gamma_1^*(gdy)$$
%$$=\int_0^1f(t,0)\frac{\partial \gamma_1}{\partial t}dt+\int_0^1f(t,0)\frac{\partial \gamma_1}{\partial t}dt$$
 %\end{proof}

Si dos curvas  $\gamma_1$ y $\gamma_2$ son homot\'opicas, entonces su diferencia ${\gamma_1-\gamma_2}$ (vista como diferencia de simplejos) es la frontera de alguna $2$-cadena.
Dicha \(2\)-cadena se puede construir a partir de cualquier homotop\'ia como en la figura \ref{fig:homot-cadena}.

\begin{figure}[h]
\centering
\subtop{\input{figuras/homot}}%
\subtop{\input{figuras/homot-simp}}
\caption{Dos curvas homot\'opicas y su diferencia vista como la frontera de una \mbox{\(2\)-cadena}.}
\label{fig:homot-cadena}
\end{figure}

\begin{corolario}
Si $\gamma$ es una curva cerrada y $\omega$ una $1$-forma tal que $d(\omega)=0$, entonces la integral
$$\int_\gamma \omega$$
\solo{} depende del tipo de homotop\'ia de $\gamma$. A las formas cuya derivada exterior se anule, las llamaremos \emph{cerradas}.
 \end{corolario}

Las formas cerradas son muy abundantes,
tal como lo afirma la siguiente proposici\'on:
\begin{proposicion}\label{prop:holo->cerrada}
Si \( f\) es una funci\'on holomorfa,
entonces \(\omega=fdz\) es un forma cerrada.
\end{proposicion}
\begin{proof}
Como ya hab\'iamos mencionado,
\(f\) es holomorfa si y \solo\ si \(d(f)=\frac{\partial f}{\partial z}dz\)
(es decir, no depende de \(d\bar z\)),
por lo que \( d(fdz)=d(f)dz=\frac{\partial f}{\partial z}dz\wedge dz=0\).
 \end{proof}

\begin{corolario}
Si $f$ es una funci\'on holomorfa, entonces la integral
$$\int_\gamma f(z)dz$$
\solo{} depende de la clase de homotop\'ia de $\gamma$.
 \end{corolario}

\begin{lema}[Lema de Poincar\'e]
Sea $\omega$ una $m$-forma con $m>0$ definida sobre $\mathbb{R}^n$ tal que $d(\omega)=0$. Entonces existe una $(m-1)$-forma $\tau$ tal que $d(\tau)=\omega$. A las formas que son la derivada exterior de otra forma, les llamaremos \emph{exactas}.
 \end{lema}

\begin{lema}
Si $C$ es una $m$-cadena con $m>0$ sobre $\mathbb{R}^2$ de tal forma que $\partial(C)=0$, entonces existe una $(m+1)$-cadena $C^\prime$ tal que $\partial (C^\prime)=C$. A las cadenas cuya frontera es $0$ les llamaremos \emph{ciclos}, y a las que son frontera de otra cadena, les llamaremos \emph{fronteras}.
 \end{lema}


\begin{teorema}[F\'ormula integral de Cauchy] 
Si $f$ es una funci\'on holomorfa en $\mathcal{U}$, $\gamma$ es una curva cerrada homot\'opica a un punto en $\mathcal{U}$ y $z\in \mathcal{U}\setminus \gamma$, entonces
$$f(z)I(\gamma,z)=\frac{1}{2\pi }\int_\gamma \frac{f(w)}{w-z}dw$$
donde $I(\gamma,z)$ es el n\'umero de vueltas de $\gamma$ con respecto a $z$.
 \end{teorema} 


\begin{teorema}[Principio del m\'aximo] 
Sea $f$ holomorfa en un disco abierto $D$  y supongamos que existe $z_0 \in D$ tal que ${\|f(z)\|\leq \|f(z_0)\|}$ para todo $z\in D$ entonces $f$ es constante en $D$.
 \end{teorema} 


\begin{teorema}[Taylor]
Si $f:\mathcal{U}\rightarrow\mathbb{C}$ es holomorfa en $\mathcal{U}$ y $D$ es un disco centrado en $z_0$ contenido en $\mathcal{U}$ entonces, para todo $z\in D$ se tiene que:
$$f(z)=\sum_{n=0}^\infty \frac{f^n(z_0)}{n!}(z-z_0)^n.$$
A una expresi\'on de la forma anterior se le llama la \emph{expansi\'on en serie de potencias} de $f$ al rededor de $z_0$.
 \end{teorema} 

\begin{lema}\label{zn-local} 
Si $\mathcal{U}\xrightarrow{f}\mathbb{C}$ es una funci\'on holomorfa definida en una vecindad del $0$ tal que $f(0)=0$ pero que no es id\'enticamente~$0$, entonces existe un entero $n>0$ y una funci\'on holomorfa $g$ tal que $f(z)=g(z)^n$ y $g^\prime(0)\neq 0$.
 \end{lema} 
\begin{proof} 
Consideremos la expansi\'on en serie de potencias de $f$ al rededor del $0$, es decir
$$f(z)= \sum_k^\infty a_kz^n$$
donde $a_k$ es el primer coeficiente no nulo. Dado que $f(0)=0$ entonces $k>0$ y como $f$ no es id\'enticamente $0$ entonces $k<\infty$ por lo que
$$f(z)=z^n\sum_{i=0}^\infty a_{n+i}z^i.$$
Si $h(z)=\sum a_{n+i}z^i$ entonces $h(0)\neq0$ por lo que en una vecindad del cero la funci\'on $h$ tiene ra\'iz $n$-\'esima bien definida,  sea ${g(z)=zh(z)^{1/n}}$ en dicha vecindad. Es claro que $f(z)=g(z)^n$ y por la regla de Leibniz $g^\prime(0)=h(0)^{1/n}\neq0$. \end{proof} 
\begin{corolario}
El conjunto de ceros de cualquier funci\'on holomorfa que no sea id\'enticamente $0$ es un conjunto discreto.
 \end{corolario}
\begin{proof}
Supongamos que  $x$ es un cero de la funci\'on $f$. Componiendo con la traslaci\'on $z\mapsto z-x$ podemos asumir que $x=0$, por lo que $f$ satisface las hip\'otesis del lema anterior y podemos garantizar la existencia de una funci\'on   $h$ tal que $f(z)=z^nh(z)$ y $h(0)\neq0$. Por continuidad, hay una vecindad $\mathcal{V}$ de $0$ en donde $h$ no se anula, y dado que el \'unico cero de $z\mapsto z^n$ es el $0$, entonces $f$ no se anula en $\mathcal{V}\setminus\{0\}$, \ie, $x$ es un cero aislado de la funci\'on.
 \end{proof}
\begin{teorema}\label{conteo-ceros}
Si $f$ es una funci\'on holomorfa en $\mathcal{U}$, $\gamma$ es una curva simple cerrada tal que $\interior(\gamma)\subseteq \mathcal{U}$ y $f$ no se anula en ning\'un punto de la imagen de $\gamma$, entonces el n\'umero de ceros de $f$ en $\interior(\gamma)$, contados con multiplicidad, es igual a:
$$\frac{1}{2\pi i}\int_\gamma \frac{f^\prime(w)}{f(w)}dw.$$
En el caso de que la integral anterior sea $1$, entonces 
$$\frac1{2\pi i}\int_\gamma \frac{wf^\prime(w)}{f(w)}dw$$
es el \'unico cero de $f$ en $\interior(\gamma)$.
 \end{teorema}
%\begin{proof}
%\todo{demostracion}
 %\end{proof}




\begin{teorema} 
Sea $f$ holomorfa en el anillo $A=\{w|a<\|z_0-w\|<b\}$, entonces para todo $z\in A$ se tiene que
$$f(z)=\sum_{n=0}^\infty a_n(z-z_0)^n + \sum_{n=1}^\infty \frac{b_n}{(z-z_0)^n}.$$
A dicha expresi\'on se le llama \emph{serie de Laurent}.
 \end{teorema} 



\begin{definicion} 
Si $f$ es holomorfa en una vecindad perforada de $p$ de la forma $\mathcal{U}\setminus \{p\}$ (en particular en un anillo degenerado al rededor de $p$) y su serie de Laurent al rededor de $p$ tiene una cantidad finita de t\'erminos de exponente negativo, diremos que $f$ es \emph{meromorfa} en $\mathcal{U}$ y que $p$ es un polo de orden $n$, donde $b_n$ es el \'ultimo coeficiente no nulo de la serie de Laurent.
 \end{definicion} 

\section*{Notas del cap\'itulo}
\subsection*{Formas diferenciales e integraci\'on}
Los resultados presentados en este cap\'itulo con respecto a formas diferenciales e integraci\'on son bien conocidos y existen muchos textos en los que se discuten. Posiblemente la exposici\'on m\'as cercana a la que hemos presentado se encuentra en \cite{spivak1965manifolds}.
\subsection*{An\'alisis complejo}
Igualmente, los resultados de an\'alisis complejo que presentamos son totalmente cl\'asicos; algunos de los numerosos textos en los que se discuten estos temas son: \cite{lascurain-compleja}, \cite{ahlfors1979complex}, \cite{stein2003complex}.

