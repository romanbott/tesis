 %! TEX root = master.tex
\chapter[Diferenciales cuadr\'aticas sobre superficies compactas][Diferenciales cuadr\'aticas \dots]{Diferenciales cuadr\'aticas sobre superficies compactas}
En este capítulo retomamos el estudio de las diferenciales cuadráticas definidas en el primer capítulo. Entre otras cosas, reformulamos la definición usando el lenguaje desarrollado en los capítulos pasados, demostramos la existencia de diferenciales cuadr\'aticas meromorfas en superficies de Riemann compactas arbitrarias y exploramos algunas de las estructuras geom\'etricas asociadas a una diferencial cuadr\'atica en una superficie de Riemann.

En lo sucesivo, $\mathcal{S}$ denota una superficie de Riemann con atlas $\mathcal{A}$.

Tomando como punto de partida la definici\'on de forma diferencial sobre una superficie y sabiendo c\'omo cambia una diferencial cuadr\'atica bajo una transformaci\'on holomorfa, podemos dar la siguiente definici\'on:

\begin{definicion}[con respecto a un atlas]
Una \emph{diferencial cuadr\'atica} $\theta$  sobre $\mathcal{S}$ es una funci\'on que a cada carta $\phi\in \mathcal{A}$ le asocia una funci\'on holomorfa $\theta_\phi$ sobre el dominio de $\phi$ de tal forma que si $\phi$ y $\psi$ son dos cartas entonces $\theta_\phi=(\theta_\psi)(\tau_{\psi\phi}^\prime)^2$. A $\theta_\phi$ se le llama la \emph{representaci\'on local} de $\theta$ en la carta $\phi$.

N\'otese que en esta definici\'on hemos suprimido el uso del s\'imbolo $dz^2$ esto es porque en este contexto, su \'unicoprop\'osito es mantenernos al tanto de que las diferenciales cuadr\'aticas \emph{no} son funciones, y  de la forma en que se transforman bajo un cambio de cordenadas.

%A partir de este momento 
 \end{definicion}
%\begin{definicion}[con respecto a un atlas]
%Una \emph{diferencial cuadr\'atica} $\theta$  sobre $\mathcal{S}$ es funci\'on que a cada carta $\phi\in \mathcal{A}$ le asocia una funci\'on holomorfa $\theta_\phi$ sobre el codominio de $\phi$ de tal forma que si $\phi$ y $\psi$ son dos cartas entonces $\theta_\phi=(\theta_\psi\circ\tau_{\psi\phi})(\tau_{\psi\phi}^\prime)^2$. A $\theta_\phi$ se le llama la \emph{representaci\'on local} de $\theta$ en la carta $\phi$.
 %\end{definicion}


Al igual que en el caso de formas diferenciales, tenemos:

\begin{proposicion-def} 
Si $\theta$ es una diferencial cuadr\'atica sobre $\mathcal{S}^\prime$ y $\mathcal{S}\xrightarrow{f}\mathcal{S}^\prime$ es una transformaci\'on holomorfa, entonces existe una \'unica diferencial cuadr\'atica $f^*(\theta)$ sobre $\mathcal{S}$ tal que si $\phi$ es una carta de $\mathcal{S}$ y $\psi$ una de $\mathcal{S}^\prime$, entonces $f^*(\theta)_\phi=(\theta_\psi\circ f)(_\psi f_\phi^\prime)^2$. Al igual que en el caso del plano complejo, esta forma recibe el nombre de \emph{imagen transpuesta \pullBack} de $\theta$ bajo $f$.
 \end{proposicion-def}
\ptodo{explicar que la dz2 es una <<mnemotecnia>>}

\ptodo{La representaci\'on de $theta$ en la carta $\phi$ es una dq $\theta d\phi$ y si $f$ es una holo entonces $f^*(\theta)_\phi=$}

Recordemos que la motivaci\'on para hablar de haces vectoriales era (entre otras cosas) que ofrecen un contexto en el que definiciones como la anterior, es decir, de <<cantidades>> que localmente son funciones escalares, pero globalmente no, tienen una representaci\'on concreta como secciones de alg\'un haz. 


Para poder aplicar estas ideas al caso de las diferenciales cuadr\'aticas, buscaremos un haz cuyas secciones holomorfas correspondan a diferenciales cuadr\'aticas.


Primero, recordemos que al construir el haz can\'onico de una superficie y demostrar que sus secciones holomorfas corresponden a $1$-formas holomorfas, hab\'ia tres puntos centrales:
\begin{itemize}
\item El haz can\'onico tiene dimensi\'on $1$, es decir, localmente las secciones son funciones holomorfas.
\item Toda carta de la superficie $\phi$ induce una trivializaci\'on del haz can\'onico, y la funci\'on de transici\'on entre las trivializaciones inducidas por dos cartas $\phi$ y $\psi$ est\'a dada por $\sigma_{\phi\psi}(p)=\tau_{\psi\phi}^\prime(p)=1/\tau_{\phi\psi}^\prime$ donde $\tau_{\psi\phi}$ es el cambio de cordenadas de $\phi$ a $\psi$.
\item Dos funciones holomorfas $s_\phi$ y $s_\psi$ definidas en el dominio de $\phi$ y $\psi$ respectivamente, al ser vistas como secciones de la trivializaci\'on inducida por $\phi$ y $\psi$, son compatibles como secciones del haz can\'onico si y solo si
$$s_\phi/\tau_{\psi\phi}^\prime=s_\psi$$
o equivalentemente
$$s_\phi=s_\psi\tau_{\psi\phi}^\prime$$
 \end{itemize}

En el caso de las diferenciales cuadr\'aticas en vez de esta \'ultima condici\'on, tenemos
$$s_\phi=s_\psi(\tau_{\psi\phi}^\prime)^2$$
lo que es equivalente a que las funciones de transici\'on del haz buscado est\'en dadas por $\sigma_{\phi\psi}(p)=1/(\tau_{\phi\psi}^\prime)^2$

Sabiendo esto, y que las funciones de transici\'on de un producto tensorial de dos haces es el producto de las funciones de transici\'on, el candidato m\'as natural para nuestros prop\'ositos es $K\otimes K$, con lo que llegamos a la segunda definici\'on de diferencial cuadr\'atica.

\begin{definicion}[secciones de un haz] Una \emph{diferencial cuadr\'atica} sobre $\mathcal{S}$ es una secci\'on holomorfa del haz $K\otimes K$.
 \end{definicion}

De la discusi\'on anterior, y en general de la discusi\'on en el caso general de haces, tenemos:

\begin{proposicion} 
Toda diferencial cuadr\'atica definida sobre un atlas induce una secci\'on del haz $K\otimes K$, y rec\'iprocamente, al tomar la representaci\'on en trivializaciones de una secci\'on de $K\otimes K$ obtenemos una diferencial cuadr\'atica definida en t\'erminos ddel atlas. Esta correspondencia es adem\'as un isomorfismo de $\mathscr{O}$-modulos.
 \end{proposicion}

A partir de este momento, no haremos distinci\'on entre las dos definiciones de diferencial cuadr\'atica; como primera aplicaci\'on de los resultados del cap\'itulo anterior, y de la definici\'on de diferenciales cuadr\'aticas en t\'erminos de secciones de un haz, tenemos:

\begin{proposicion} 
Toda superficie de Riemann compacta admite diferenciales cuadr\'aticas meromorfas no constantes, adem\'as, la dimensi\'on del conjunto de diferenciales cuadr\'aticas holomorfas como espacio vectorial complejo es finita.
 \end{proposicion}
\begin{proof}
Esto solo es una aplicaci\'on del teorema \ref{teo-finitud} al caso del haz ${K\otimes K}$.
 \end{proof}





\section[Aspectos geom\'etricos de las diferenciales cuadr\'aticas][Aspectos geom\'etricos \dots]{Aspectos geom\'etricos de las diferenciales cuadr\'aticas}

Ahora que hemos formalizado la noci\'on de diferencial cuadr\'atica, es momento de formalizar muchas de las nociones de las que hablamos en el cap\'itulo \ref{difplano}.


En esta secci\'on, $\mathcal{S}$ denota una superficie de Riemann y $\theta$ una diferencial cuadr\'atica sobre $\mathcal{S}$.






\subsection{Coordenadas normales}

Si $p\in \mathcal{S}$ es un punto regular de $\theta$ (es decir, no es un cero ni un polo) entonces de manera totalmente an\'aloga a la proposici\'on \ref{clasificacion-local-regular} tenemos:

\begin{proposicion-def} 
Existe una vecindad $\mathcal{U}$ de $p$ y una $1$-forma holomorfa $\omega$ sobre $\mathcal{U}$ de tal forma que $\omega\otimes\omega=\theta$ y adem\'as la funci\'on:
$$q\mapsto \int_p^q \omega$$
es una funci\'on holomorfa invertible, donde la integraci\'on es sobre cualquier curva lisa que una $p$ a $q$ y que est\'e contenida en $\mathcal{U}$.

Como dicha funci\'on es invertible, puede ser tomada como una carta centrada en $p$, y adem\'as, $\theta$ representada en esta carta es la diferencial cuadr\'atica $1dz^2$.
A una carta como \'esta se le llama \emph{cordenada normal} centrada en $p$.
 \end{proposicion-def}

Pr\'acticamente todas las construcciones geom\'etricas asociadas a una diferencial cuadr\'atica se construyen <<copiando>> la estructura del plano complejo bajo cordandas normales, por lo que resulta importante entender c\'omo son los cambios de cordenadas entre dos cordendas de este tipo; en lo sucesivo nos referiremos al conjunto de todas las cordendas normales como \emph{el atlas normal} asociado a la diferencial cuadr\'atica.


\begin{proposicion}\label{cambiosNormales}
Si $\phi$ y $\psi$ son dos cordendas normales, entonces el cambio de cordenadas $\tau_{\psi\phi}$ tiene derivada constante $\pm 1$, por lo que es una traslaci\'on, o la composici\'on de una traslaci\'on con una reflexi\'on con respecto a alguna recta vertical.
 \end{proposicion}
\begin{proof}
La demostraci\'on es totalmente elemental y bien podr\'ia formar parte del cap\'itulo \ref{difplano}, sin embargo, es en este contexto en el que realmente se vuelve relevante.

Si $\phi$ y $\psi$ son cordenadas normales, entonces $\phi^*(\theta)=1dz^2=\psi^*(\theta)$, por lo que $(\tau_{\psi\phi}^\prime)^2dz^2=\tau_{\psi\phi}^*(1dz^2)=1dz^2$, es decir:
$$(\tau_{\psi\phi}^\prime)^2=1$$
y finalmente $\tau_{\psi\phi}^\prime=\pm 1$.
 \end{proof}

La primera estructura que definiremos en la superficie $\mathcal{S}$ por medio de una diferencial cuadr\'atica es la \emph{foliaci\'on horizontal}:

\begin{definicion}
Sea $\mathcal{S}$ una superficie lisa. Una \emph{foliaci\'on} de $\mathcal{S}$ (de dimensi\'on $1$) es un atlas $\mathcal{F}$ de $\mathcal{S}$ que cumple que para cualesquiera dos cartas $\mathcal{U}\xrightarrow{\phi}\mathcal{U}^\prime,{\mathcal{V}\xrightarrow{\psi}\mathcal{V}^\prime\in\mathcal{F}}$  el cambio de cordenadas $\tau_{\psi\phi}=\psi\circ\phi^{-1}$ es de la forma:
$$\tau_{\psi\phi}:\phi(\mathcal{U}\cap\mathcal{V})\rightarrow\psi(\mathcal{U}\cap\mathcal{V})$$
$$(x,y)\mapsto(f(x,y),g(y))$$
donde $f$ y $g$ son funciones diferenciables.
En otras palabras, para cualquier $t\in \mathbb{R}$ la curva $\{\phi_2(x)=t\}$ coincide con la curva $\{\psi_2(x)=g(t)\}$ en la interseccion $\mathcal{U}\cap\mathcal{V}$.

A un conjunto conexo y maximal $\mathcal{H}$ de $\mathcal{S}$ con respecto a la propiedad de que para cualquier carta $\phi\in\mathcal{F}$ se cumple que $\phi_2(x)$ es constante en $\mathcal{H}$, le llamaremos una \emph{hoja} de la foliaci\'on.
 \end{definicion}

\begin{ejemplo}
Cualquier abierto $\mathcal{U}\subseteq \mathbb{C}$ tiene una foliaci\'on can\'onica inducida por el atlas $\mathcal{F}=\{id_\mathcal{U}\}$. A esta foliaci\'on le llamaremos la \emph{foliaci\'on horizontal} de $\mathcal{U}$ pues sus hojas son exactamente las rectas horizontales.
\ptodo{identidades}
 \end{ejemplo}

Por la proposici\'on \ref{cambiosNormales} es claro que la foliaci\'on horizontal can\'onica de $\mathbb{C}$ se preserva bajo cambios de cordenadas del atlas normal, por lo que el altas normal induce una foliaci\'on sobre el complemento de los puntos cr\'iticos de la diferencial cuadr\'atica, es esta foliaci\'on a lo que nos hemos referido con anterioridad como la <<estructura>> de las trayectorias horizontales de una diferencial cuadr\'atica.

\begin{definicion}
Una \emph{m\'etrica hermitiana} $h$ sobre $\mathcal{S}$, es una familia de productos hermitianos $h_p$ sobre $T_p\mathcal{S}$ para cada $p\in \mathcal{S}$ que satisfacen:

Si $X$ y $Y$ son dos campos vectoriales sobre $\mathcal{S}$ entonces $h(X_p,Y_p)_p$ es una funci\'on lisa sobre $\mathcal{S}$.\footnote{Una noci\'on relacionada es la de \emph{m\'etrica riemanniana}, en este caso los productos son productos interiores sobre el plano tangente visto como espacio vectorial real.}
 \end{definicion}

En cierta forma, las m\'etricas son los objetos que marcan el punto de partida entre la topolog\'ia diferencial y la geometr\'ia diferencial; una vez que una superficie ha sido dotada de una m\'etrica, se pueden definir nociones como: longitud de curvas, el \'area de la superficie, la curvatura de la superficie, geod\'esicas, etc.

Dado que definir todas estas nociones nos llevar\'ia demasiado lejos, solo hablaremos de longitud de curvas y del concepto de <<ausencia de curvatura>>.

\begin{definicion}
Si  $h$ es una m\'etrica hermitiana y $\gamma$ una curva definida sobre un intervalo $[a,b]$, entonces la \emph{longitud de $\gamma$} con respecto a $h$ es el n\'umero real
$$\int_a^b \sqrt{h_{\gamma(t)}(\gamma^\prime(t),\gamma^\prime(t))}dt$$
 \end{definicion}
\begin{observacion}
Es claro como extender esta noci\'on a $1$-simplejos.
 \end{observacion}

\begin{definicion}
Sean $\mathcal{S}$ y $\mathcal{S}^\prime$ dos superficies lisas y $h$ y $h^\prime$ dos m\'etricas hermitianas sobre $\mathcal{S}$ y $\mathcal{S}^\prime$ respectivamente.
Un difeomorfismo $\phi:\mathcal{S}\rightarrow\mathcal{S}^\prime$ es una \emph{isometr\'ia} (con respecto a $h$ y $h^\prime$) si para cualesquiera dos vectores $v,w\in T_p\mathcal{S}$ se cumple que $h_p(v,w)=h_{\phi(p)}^\prime(D_p\phi(v),D_p(\phi(w))$, es decir, si la diferencial es una isometr\'ia entre los espacios tngentes correspondientes. Si existe una isometr\'ia entre dos superficies, diremos que estas son \emph{isom\'etricas}.

Decimos que una superficie junto con una m\'etrica hermitiana $h$ es \emph{plana} si es localmente isom\'etrica al plano complejo con la m\'etrica est\'andar.
 \end{definicion}

Notemos que los cambios de cordenadas de cordenadas normales son isometr\'ias del plano con la m\'etrica hermitiana est\'andar, por lo que podemos definir una m\'etrica hermitiana en la superficie de la siguiente manera:

\begin{proposicion-def} 
Sea $p\in \mathcal{S}$ y $\phi$ y $\psi$ dos sistemas de cordenadas normales al rededor de $p$, entonces, para cualesquiera dos vectores $v,w\in T_p(\mathcal{S})$ se tiene que:
$$\langle D_p\phi(v),D_p\phi(w) \rangle =\langle D_p\psi(v),D_p\psi(w) \rangle $$
donde $\langle\ ,\ \rangle$ denota el producto hermitiano est\'andar de $\mathbb{C}$.

De esta forma, podemos definir una m\'etrica hermitiana $h$ sobre el complemento de los puntos singulares de la diferencial cuadr\'atica mediante:
$$h_p(v,w):=\langle D_p\phi(v),D_p\phi(w) \rangle$$
donde $\phi$ es cualquier cordenada normal.

A esta m\'etrica le llamaremos la \emph{m\'etrica asociada a la diferencial cuadr\'atica}.
 \end{proposicion-def}

Es claro que las cordenadas normales son isometr\'ias, por lo que $\mathcal{S}$ junto con la m\'etrica asociada a la diferencial es una superficie plana.

\begin{definicion}
Si $h$ es una m\'etrica hermitiana sobre $\mathcal{S}$, entonces decimos que una curva $\gamma$ es una geod\'esica (con respecto a $h$) si localmente es una curva de m\'inima longitud, \ie si para cualquier punto $t$ en el dominio de $\gamma$ existe un intervalo abierto $\mathcal{I}$ que contiene a $t$ y tal que para cualesquiera dos puntos $r<s \in \mathcal{I}$ y cualquier $1$-simplejo $\rho$ tal que $\delta(\rho)=\gamma(s)-\gamma(r)$ se tiene que 
$$l(\rho)>l(\gamma_r^s)$$ donde $\gamma_r^s$ denota la restricci\'on de $\gamma$ al intervalo $[r,s]$.
 \end{definicion}

Es un resultado cl\'asico que las geod\'esicas del plano con la m\'etrica est\'andar son exactamente las rectas. Usando esto y que las cordenadas normales son isometr\'ias obtenemos:

\begin{proposicion} 
Toda trayectoria horizontal de la diferencial cuadr\'atica es una geod\'esica con respecto a la m\'etrica inducida por \'esta.
 \end{proposicion}

\ptodo{descomposici\'on en celdas y cilindros}



\section{Las cordenadas de periodos}\label{cordenadasPeriodos}

\Todo{discusi\'on de los periodos en el contexto cl\'asico}
\subsection{Periodos de diferenciales abelianas}

Antes del desarrollo de la cohomolog\'ia de de Rham, la manera usual en la que se trabajaba con diferenciales abelianas, era caracteriz\'andolas mediante el valor que toma su integral a lo largo de curvas cerradas arbitrarias.

M\'as espec\'ificamente, dado que una diferencial abeliana es una forma cerrada, su integral \solo\ depende de la clase de homolog\'ia de la curva con respecto a la que se integra. Adem\'as, el hecho de que la integral sea aditiva en el dominio, nos permite restringirnos a integrar la diferencial abeliana sobre una base del primer grupo de homolog\'ia de la superficie:

\begin{definicion}
Sea $\mathcal{S}$ una superficie de Riemann y $\omega$ una diferencial abeliana sobre $\mathcal{S}$. Sea $\{\beta_i\}$ una base para $\H_i(\mathcal{S})$, entonces definimos el $i$-\'esimo \emph{periodo} de $\omega$ como el n\'umero complejo $\int_{\beta_i}\omega$.
 \end{definicion}

\begin{proposicion}
Si la superficie $\mathcal{S}$ es compacta, los periodos determinan un\'ivocamente a las diferenciales abelianas, es decir, si $\omega$ y $\eta$ son dos diferenciales abelianas con los mismos periodos, entonces son iguales.
 \end{proposicion}
\begin{proof}
Esta afirmaci\'on es equivalente al hecho de que la \'unica diferencial abeliana cuyos periodos son todos cero es la diferencial cero.

Supongamos que $\omega$ es una diferencial abeliana cuyos periodos son cero. Por el teorema de de Rham, concluimos que la clase de cohomolog\'ia de $\omega$ es cero, es decir, $\omega$ es una forma exacta, por lo que existe una funci\'on $f$ definida en $\mathcal{S}$ tal que $df=\omega$. Esto \'ultimo implica que $f$ es holomorfa, pues localmente, su derivada es holomorfa. Como la superficie es compacta, concluimos que $f$ es constante y $\omega=df=0$.
 \end{proof}

\subsection{La ra\'iz cuadrada de una diferencial cuadr\'atica}\label{raiz-global}

Para definir la noci\'on an\'aloga a los periodos de las diferenciales abelianas en el contexto de las diferenciales cuadr\'aticas, una posiblidad es recurrir a una <<ra\'iz cuadrada>> de la diferencial cuadr\'atica.


Como ya hab\'iamos mencionado en el cap\'itulo \ref{difplano} en algunos casos es posible encontrar una <<ra\'iz cuadrada>> para ciertas diferenciales cuadr\'aticas; en este contexto, este problema se podr\'ia enunciar como:

\textbf{Problema.} Dada una diferencial cuadr\'atica meromorfa $\theta$ sobre una superficie de Riemann, dar condiciones necesarias y suficientes para que exista una diferencial abeliana $\omega$ tal que $\omega\otimes\omega=\theta$ en toda la superficie.

La soluci\'on del problema anterior involucra dos aspectos: uno local y otro global. El problema local es exactamente lo que se ha discutido en el cap\'itulo \ref{difplano}, en donde encontramos que cerca de un punto arbitrario, se puede encontrar una ra\'iz cuadrada (localmente) \'unicamente si el punto sea no es un cero o polo de orden impar de $\theta$.

El problema global es un poco m\'as sutil:

\begin{ejemplo}
Consideremos la diferencial cuadr\'atica $\theta=zdz^2$ en el abierto $\mathcal{U}=\{z|\|z\|>\frac12\}$ de $\mathbb{C}$. Es claro que $\theta$ no tiene ceros ni polos, por lo que no hay ninguna obstrucci\'on para encontrar ra\'ices cuadradas de $\theta$ localmente. Sin embargo, es claro que el mismo argumento utilizado en \ref{cuadratica-no-abel} para demostrar que la diferencial cuadr\'atica $zdz^2$ en $\mathbb{C}$ no tiene ra\'iz cuadrada (global) funciona en este caso, por lo que $\theta$ tampoco tiene ra\'iz cuadrada global.
\missingfigure{zdz2 en U}
 \end{ejemplo}

\begin{proposicion} 
Sea $\theta$ una diferencial cuadr\'atica sobre la superficie $\mathcal{S}$, entonces las siguientes condiciones son equivalentes:
\begin{enumerate}
 \item Existe una diferencial abeliana $\omega$ que satisface $\omega\otimes\omega=\theta$.\label{uno}
\item Existe un subatlas $\mathcal{A}^\prime\subseteq\mathcal{A}$ del atlas normal asociado a $\theta$ que cumple que los cambios de cordenadas $\tau_{\psi\phi}$ son traslaciones para cualesquiera $\psi,\phi\in\mathcal{A}^\prime$.\label{dos}
%\item Existe un campo vectorial holomorfo $\mathscr{X}$ sobre el complemento de los ceros y polos de $\theta$ cuyas curvas integrales son trayectorias horizontales de $\theta$.\label{tres}
 \end{enumerate}
 \end{proposicion}
\begin{proof}
\ref{uno}$\Rightarrow$ \ref{dos}: Supongamos que $\omega$ es una diferencial abeliana que satisface $\omega\otimes\omega=\theta$. Entonces, si $\phi$ es una cordenada normal de $\theta$, tenemos que:
$$\phi^*(\theta)=\phi^*(\omega\otimes\omega)=\phi^*(\omega)\otimes\phi^*(\omega)$$
pero como $\phi^*(\theta)=1dz^2$, concluimos que $\phi^*(\omega)=\pm dz$.

Sea $\mathcal{A}^\prime=\{\phi\in\mathcal{A}| \phi^*(\omega)=dz\}$, que es un atlas pues si $p\in\mathcal{S}$ es un punto regular de $\theta$ entonces existe una cordenada normal centrada en $p$ y es claro que $\phi\in\mathcal{A}^\prime$ o bien $-\phi\in\mathcal{A}^\prime$.
De manera an\'aloga a la demostraci\'on de la proposici\'on \ref{cambiosNormales} se puede ver que los cambios de cordenadas del atlas $\mathcal{A}^\prime$ son traslaciones.

\ref{dos}$\Rightarrow$\ref{uno}: Como en cada cordenada normal la representaci\'on de $\theta$ es $1dz^2$, entonces al menos localmente, tenemos que $\theta_\phi=1dz^2=dz\otimes dz$. Si $\psi$ es una traslaci\'on del plano complejo, entonces es claro que $\psi^*(dz)=dz$, por lo que las formas $\omega_\phi:=dz$ definidas en el dominio de cordenadas normales $\phi\in\mathcal{A}^\prime$ forman una diferencial abeliana bien definida en el complemento de los puntos cr\'iticos de $\theta$ a la que llamaremos $\omega$. Como localmente tenemos que $\theta_\phi=\omega_\phi\otimes\omega_\phi$ entonces globalmente se cumple que $\theta=\omega\otimes\omega$.
 \end{proof}

La proposici\'on anterior da una soluci\'on completa al problema antes propuesto y en particular nos permite dar una definici\'on de \emph{periodo} para diferenciales cuadr\'aticas que satisfagan alguna de las condiciones de la proposici\'on:

\begin{definicion}[provisional]
Sea $\theta$ una diferencial cuadr\'atica sobre $\mathcal{S}$ con ra\'iz cuadrada $\omega$, $\mathcal{S}^\circ=:\mathcal{S}\setminus \{p|p \text{ es un polo de }\theta\}$ y $\{\beta_i\}$ una base para $\H_1(\mathcal{S}^\circ)$ el primer grupo de homolog\'ia de $\mathcal{S}^\circ$. Entonces el \emph{$i$-\'esimo periodo} de $\theta$ es el n\'umero complejo $\int_{\beta_i}\omega$.
 \end{definicion}

\begin{observacion}
Los periodos de la diferencial cuadr\'atica $\theta$ dependen de la elecci\'on de la ra\'iz cuadrada $\omega$, y si cambiamos $\omega$ por $-\omega$, los periodos tambi\'en cambian de signo.
 \end{observacion}




%Si el caso local est\'a obstruido, es m\'as complicado, ejemplo fundamental, resolver localmente $zdz^2$. esto nos lleva a un cubriente de dos hojas donde si hay raiz cuadr\'ada.





\subsection{La cubierta espectral}\label{cubierta-espectral}

En la subsecci\'on anterior, motivados por la idea de definir los <<periodos>> de una diferencial cuadr\'atica, encontramos una caracterizaci\'on de aquellas diferenciales cuadr\'aticas que tienen ra\'iz cuadrada. En esta ocasi\'on tomaremos otra ruta que permite extender la definici\'on de periodo al caso en el que la diferencial cuadr\'atica no tenga ra\'iz cuadrada. Comenzaremos con la versi\'on local de esta construcci\'on.

\begin{proposicion} 
Sea $\theta$ una diferencial cuadr\'atica definida en una vecindad $\mathcal{U}$ del cero en el plano complejo y que tiene un cero o polo de orden impar en el cero. Entonces existe un cubriente ramificado de una vecindad posiblemente m\'as peque\~na del cero
$$\pi:\mathcal{S}\rightarrow\mathcal{V}$$
que cumple que $\pi^*(\theta)$ tiene ra\'iz cuadrada global.
 \end{proposicion}
%\begin{proof}
%La demostrac\'on es totalmente elemental. Las hip\'otesis implican que 
 %\end{proof}

\begin{ejemplo}
Sea $\theta:=zdz^2$ definida en todo el plano complejo. En este caso, el cubriente ramificado est\'a dado por:
$$\pi:\mathbb{C}\rightarrow\mathbb{C}$$
$$z\mapsto z^2$$
y se tiene que $\pi^*(zdz^2)=z^4dz^2=(z^2dz^2)(z^2dz^2)$.
\missingfigure{cubriente local zdz2}
 \end{ejemplo}




%Motivar buscando un caso global de la resoluci\'on de $zdz^2$ globalmente.

%Haces torcidos en un punto, secciones de los haces torcidos, secciones meromorfas, divisores, 


Antes de considerar el caso global sobre una superficie de Riemann arbritraria, comencemos con el caso m\'as simple en el que la diferencial cuadr\'atica es una diferencial sobre $\mathbb{C}$, es decir $\theta=f(z)dz^2$ donde $f$ es una funci\'on meromorfa sobre el plano complejo $\mathbb{C}$.

En este caso, considerar el cubriente del ejemplo anterior, es decir $z\mapsto z^2$ no resuelve el problema, pues si $f$ tiene alg\'un cero o polo de orden impar en un punto distinto del cero, entonces en ese punto el cubriente es de hecho un biholomorfismo local, por lo que la diferencial cuadr\'atica inducida por el cubriente sigue teniendo un cero o polo de orden impar en la preimagen del punto.\ptodo{}

Por otro lado, recordemos que encontrar una ra\'iz cuadrada para la diferencial $\theta$ es equivalente a encontrar una ra\'iz cuadrada para la funci\'on $f$, pues si $g$ es una funci\'on meromorfa sobre el plano complejo que satisface $g(z)^2=f(z)$ para todo n\'umero complejo $z$, entonces la diferencial abeliana $\omega:=g(z)dz$ satisface $\omega\otimes\omega=\theta$.



Consideremos la funci\'on $h:\mathbb{C}\times \mathbb{C}\rightarrow \mathbb{C}$ dada por $h(z,w)=f(z)-w^2$. Es claro que $h$ satisface las hip\'otesis del teorema de la funci\'on impl\'icita, por lo qe $\hat{\mathcal{S}}:=h^{-1}(0)$ es una superficie de Riemann y adem\'as $\pi_1:\mathbb{C}\times\mathbb{C}\rightarrow\mathbb{C}$ se restringe a una transformaci\'on holomorfa de $\mathcal{S}$ sobre $\mathbb{C}$, ramificada exactamente sobre los ceros de $f$ y que denotaremos $\pi$. 


Sea $\omega=:\pi_2 \pi^*(dz)$, que es una diferencial abeliana pues $\pi_2$ es una funci\'on holomorfa sobre $\hat{\mathcal{S}}$. Si $(z_0,w_0)$ es un punto de $\hat{\mathcal{S}}$ tal que $w_0=\pm\sqrt{f(z_0)}\neq0$ entonces se tiene que $\partial_w h(z_0,w_0)=2w_0\neq0$, por lo que el teorema de la funci\'on impl\'icita garantiza que $\pi_1$ es una carta en una vecindad de $(z_0,w_0)$. No es dif\'icil ver que en esta carta  se tiene que $\omega\otimes\omega=\pi^*(\theta)$. Como el conjunto de puntos en los que $w_0$ es distinto de cero es un abierto denso de $\hat{\mathcal{S}}$, entonces la igualdad anterior sigue siendo v\'alida en todo $\hat{\mathcal{S}}$, es decir, $\pi^*(\theta)=\omega\otimes\omega$.



Sea $\mathcal{S}$ una superficie de Riemann compacta, y $\theta$ una diferencial cuadr\'atica meromorfa. Digamos que $$\divisor(\theta)=\sum_{i=1}^{r}m_ip_i-\sum_{i=1}^sn_iq_i$$ es el divisor de $\theta$.

Sea $D=\sum\lfloor\frac{m_i}2 \rfloor p_i-\sum\lceil\frac{n_i}{2} \rceil q_i$ es decir, $\divisor(\theta)-2D$ toma el valor $1$ en los ceros y polos de orden impar y $0$ en los dem\'as puntos.

Si consideramos el haz $K(D):=K\otimes L(D)$ entonces es claro que $\theta$ corresponde a una secci\'on \emph{holomorfa} de $K(D)\otimes K(D)\cong (K\otimes L(D))\otimes (K\otimes L(D))\cong(K\otimes K)\otimes L(2D)$ pues $\divisor(\theta)\geq 2D$, dicha secci\'on la denotaremos por $\tilde{\theta}$.

Por la manera en que se defini\'o $D$, y el hecho de que $\tilde{\theta}$ tiene divisor $\divisor(\theta)-2D$ concluimos que $\tilde{\theta}$ tiene exactamente un cero simple en cada polo o cero de orden impar de $\theta$.

Sea $$\hat{\mathcal{S}}:=\{(p,v)|v\otimes v = \tilde{\theta}_p\}\subseteq K(D)$$

Denotemos por $\pi$ la restricci\'on de la proyecci\'on del haz $K(D)$ sobre $\mathcal{S}$ y por $\tau$ la multiplicaci\'on fibra a fibra por $-1$. Usando esta notaci\'on demostraremos:

\begin{teorema} 
Si $\theta$ es una diferencial cuadr\'atica entonces existe un cubriente ramificado de dos hojas $\pi:\hat{\mathcal{S}}\rightarrow \mathcal{S}$, ramificado exactamente en los ceros y polos de orden impar, en la cual $\pi^*(\theta)$ es el cuadrado de una $1$-forma $\omega$, y adem\'as existe una involuci\'on $\tau$ que satisface $\tau^*(\omega)=-\omega$ y $\pi\circ\tau=\pi$. A dicho cubriente le llamaremos la \emph{cubierta espectral} de $\mathcal{S}$ asociada a $\theta$.
 \end{teorema}
\begin{proof}
Comencemos demostrando que $\hat{\mathcal{S}}$ es efectivamente una superficie de Riemann.

Sea $(p,v)$ un punto de $\hat{\mathcal{S}}$ y $\phi:\pi^{-1}(\mathcal{U})\rightarrow \mathcal{U}\times \mathbb{C}$ una trivializaci\'on de $K(D)$ en una vecindad de $p\in\mathcal{S}$. Dicha trivializaci\'on induce una trivializaci\'on $\phi\otimes \phi$ de $K(D)\otimes K(D)$. Para evitar complicar excesivamente la notaci\'on, asumiremos adem\'as que $\mathcal{U}$ ha sido identificado mediante una carta de $\mathcal{S}$ centrada en $p$ con un abierto del plano complejo. Sea $\tilde{\theta}_\phi$ la representaci\'on de $\tilde{\theta}$ en la trivializaci\'on $\phi\otimes\phi$. De esta forma tenemos que $\phi(\hat{\mathcal{S}}\cap \pi^{-1}(\mathcal{U}))=\{(z,w)| w^2=\tilde{\theta}_\phi(z)\}$. La construcci\'on de $D$ y $\tilde{\theta}$ garantizan que $(\tilde\theta)_\phi$ es una funci\'on \emph{holomorfa} con ceros simples, por lo que, al igual que en la construcci\'on anterior, podemos usar el teorema de la funci\'on impl\'icita para demostrar que $\phi(\hat{\mathcal{S}}\cap\pi^{-1}(\mathcal{U}))$ es una superficie de Riemann.
 \end{proof}

\begin{proposicion} 
La cubierta espectral de $\theta$ tiene caracter\'istica de Euler
$$\chi(\hat{\mathcal{S}})=2\chi(\mathcal{S}) -\#\{\text{ceros y polos de orden impar}\}.$$\attention
 \end{proposicion}




%\subsection{Los periodos de una diferencial cuadr\'atica}

%\begin{teorema} 
%el mapeo de periodos es inyecivo
 %\end{teorema}


%\begin{definicion}
%diferencial GMN
 %\end{definicion}





\section*{Notas del cap\'itulo}
\subsection*{Diferenciales cuadr\'aticas}
Al igual que en el cap\'itulo \ref{difplano}, la referencia principal es \cite{strebel-quadratic}.

\subsection*{La cubierta espectral}
La construcci\'on de la cubierta espectral para una diferencial cuadr\'atica se puede consultar en \cite{bridgeland2013quadratic}.
